
# Visual Studio Marketplace #
[![Version](https://img.shields.io/visual-studio-marketplace/v/marcinbar.vscode-gothiconline.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-gothiconline)
[![Installs](https://img.shields.io/visual-studio-marketplace/i/marcinbar.vscode-gothiconline.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-gothiconline)
[![Rating](https://img.shields.io/visual-studio-marketplace/r/marcinbar.vscode-gothiconline.svg)](https://marketplace.visualstudio.com/items?itemName=marcinbar.vscode-gothiconline)

# Open VSX
[![Version](https://img.shields.io/open-vsx/v/marcinbar/vscode-gothiconline)](https://open-vsx.org/extension/marcinbar/vscode-gothiconline)
[![Installs](https://img.shields.io/open-vsx/dt/marcinbar/vscode-gothiconline)](https://open-vsx.org/extension/marcinbar/vscode-gothiconline)
[![Rating](https://img.shields.io/open-vsx/rating/marcinbar/vscode-gothiconline)](https://open-vsx.org/extension/marcinbar/vscode-gothiconline)

# How to use
Run VS Code
Click File>Open folder... and select folder with your G2O_Server.exe file 
You are ready to go;

# IntelliSense for Gothic Online #
Support IntelliSense for function used in Gothic Online

# IntelliSense Features
* Completions.
* Hover.
* SignatureHelp.
* Color Picker.
* Diagnostic.
* InlayHints.
* CodeAction.
* Multi API import (direct link to json, or gitlab eg. https://gitlab.com/GothicMultiplayerTeam/modules/squirrel-template).

# License
You are free to use this in any way you want, in case you find this useful or working for you but you must keep the copyright notice and license. (MIT)

# Credits
* F5 Anything from discretegames https://marketplace.visualstudio.com/items?itemName=discretegames.f5anything
* deepmerge from TehShrike https://github.com/TehShrike/deepmerge