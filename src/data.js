const vscode = require('vscode')
let urlstable = 'https://gothicmultiplayerteam.gitlab.io/docsapi/stable.json'
let urldev = 'https://gothicmultiplayerteam.gitlab.io/docsapi/dev.json'
let urlverion = 'https://bitbucket.org/marcinbar91/vscode-gothiconline/raw/master/src/version.json'


const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)

let DecorationsClient = vscode.window.createTextEditorDecorationType({
    fontWeight: "bold",
    color: configuration.get('DecorationsClient'),
})
let DecorationsServer = vscode.window.createTextEditorDecorationType({
    fontWeight: "bold",
    color: configuration.get('DecorationsServer'),
})
let DecorationsShared = vscode.window.createTextEditorDecorationType({
    fontWeight: "bold",
    color: configuration.get('DecorationsShared'),
})

const ClientServerDiagnostics = vscode.languages.createDiagnosticCollection('ClientServerDiagnostic')
const DeprecatedDiagnostics = vscode.languages.createDiagnosticCollection('DeprecatedDiagnostic')
const EventDiagnostics = vscode.languages.createDiagnosticCollection('EventDiagnostic')
let docs = {}
let docsDeprected = {}

var EventDiagnosticregparams = /(?:(?!(?:[ ]{2,}|\]|,)).)+/g
var EventDiagnosticreg = /addEventHandler\s*\("(.*)",\s*(?:function\s*\((.*)|(.*))\)/g;
var provideFuncCompletionItemsparamsRegex = /\s*\(\s*"{1}/
var provideXMLCompletionItemsreg = /(<(script|import|items|mds|wayfile)(?:.*)?\s*src=)"([0-9A-z\/\-.]*)"/g;
var provideSignatureHelpreg = /([\w.]+)\s?\(/g;
var provideInlayHintsreg2 = /([\w.]+)\s?\(/g;
var provideInlayHintsregforparameters = /(?:function\s\(.*\)|[^,(,)]+(?:\((?:[^,()]|\(|\))*\))*)+/g;
var provideDocumentLinkreg = /([\w.]+)/g;
var DiagnosticReg = /([\w.]+)(?=\s?\()/g;

module.exports = {
    urlstable, urldev, urlverion,
    docs, docsDeprected, ClientServerDiagnostics, DeprecatedDiagnostics, EventDiagnostics, DecorationsShared, DecorationsServer, DecorationsClient,
    EventDiagnosticregparams, EventDiagnosticreg, provideFuncCompletionItemsparamsRegex, provideXMLCompletionItemsreg, provideSignatureHelpreg,provideInlayHintsreg2,provideInlayHintsregforparameters, provideDocumentLinkreg, DiagnosticReg
}