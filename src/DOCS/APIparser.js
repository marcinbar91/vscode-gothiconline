const vscode = require('vscode');
const statusbar = require('../statusbar');

class APIparser {
    ServerCompletion = [];
    ClientCompletion = [];
    SharedCompletion = [];
    ServerInLine = [];
    ClientInLine = [];
    SharedInLine = [];
    ServerHover = [];
    ClientHover = [];
    SharedHover = [];
    ServerSignature = [];
    ClientSignature = [];
    SharedSignature = [];
    AddEventHandlerEventServer = [];
    AddEventHandlerEventClient = [];
    Constants = [];
    aDOT = [];
    Deprected = [];
    version2 = "";
    DOCS2 = [];

    constructor(docs) {
        this.version2 = docs.version;
        this.DOCS2 = docs;
        if (docs.functions) {
            for (const element of docs.functions) {
                this.Get(element, undefined, vscode.CompletionItemKind.Function)
            }
        }
        if (docs.classes) {
            for (const element of docs.classes) {
                this.Get(element, undefined, vscode.CompletionItemKind.Class);
                for (const element2 of element.methods) {
                    this.Get(element2, element, vscode.CompletionItemKind.Method)
                }
                for (const element2 of element.properties) {
                    this.Get(element2, element, vscode.CompletionItemKind.Property)
                }
            }
        }
        if (docs.events) {
            for (const element of docs.events) {
                this.Get(element, undefined, vscode.CompletionItemKind.Event);
            }
        }
        if (docs.constants) {
            for (const element of docs.constants) {
                this.GetConstants(element);
            }
        }
        if (docs.globals) {
            for (const element of docs.globals) {
                this.Get(element, undefined, vscode.CompletionItemKind.Variable);
            }
        }
        statusbar.updateStatusBarVersion(docs.version)
    }

    Get(element, element2, kind) {
        let name;
        let side;
        let description;
        let classs;
        if (kind == vscode.CompletionItemKind.Function || kind == vscode.CompletionItemKind.Event || kind == vscode.CompletionItemKind.Variable) {
            name = element.name;
            side = element.side;
            description = element.description
            classs = name;
        }
        else if (kind == vscode.CompletionItemKind.Class) {
            name = element.definition.name;
            side = element.definition.side;
            description = element.definition.description
            classs = name;
            if (element.constructors.length > 1) {
                let t = element;
                for (const element3 of element.constructors) {
                    t.constructors = [element3]
                    this.Get(t, undefined, kind)
                }
                return;
            }
        }
        else if (kind == vscode.CompletionItemKind.Method) {
            name = element.name;
            side = element2.definition.side;
            description = element2.definition.description;
            classs = element2.definition.name + "." + element.name;
        }
        else if (kind == vscode.CompletionItemKind.Property) {
            name = element.name;
            side = element2.definition.side;
            description = element2.definition.description;
            classs = element2.definition.name + "." + element.name
        }
        let head = this.GetHead(element, kind);
        let documentation = this.GetDocumentation(element, kind);
        let Completion = this.GetCompletion(name, head, documentation, kind, element.deprecated)
        let Hover = this.GetHover(classs, head, documentation)
        let signatureInformation = undefined
        if (kind != vscode.CompletionItemKind.Variable) {
            signatureInformation = this.GetSignature(element, classs, head, description, kind)
        }
        if (kind != vscode.CompletionItemKind.Method && kind != vscode.CompletionItemKind.Property) {
            this.ToSIDE(side, kind, Completion, Hover, signatureInformation, new vscode.InlineCompletionItem(name))
        }
        else {
            this.ToAdot(element2.definition, Completion, Hover, signatureInformation)
        }
    }

    GetConstants(element) {
        let obj = {
            category: element.category,
            completion: [],
            hover: [],
            side: element.side
        };
        for (const element2 of element.elements) {
            let Completion = this.GetCompletion(element2.name, element2.name, element2.description, vscode.CompletionItemKind.Constant, element2.deprecated)
            let Hover = this.GetHover(element2.name, element2.name, element2.description)
            this.ToSIDE(element.side, vscode.CompletionItemKind.Constant, Completion, Hover)
            obj.completion.push(Completion);
            obj.hover.push(Hover);
        }
        this.Constants.push(obj)
    }

    ToAdot(element, Completion, Hover, signatureInformation) {
        for (let i = 0; i < this.aDOT.length; i++) {
            if (this.aDOT[i].name == element.name && this.aDOT[i].side == element.side) {
                this.aDOT[i].Completion.push(Completion);
                this.aDOT[i].Hover.push(Hover);
                this.aDOT[i].SignatureInformation.push(signatureInformation);
                return;
            }
        }
        this.aDOT.push({
            static: element.static,
            side: element.side,
            name: element.name,
            Completion: [Completion],
            Hover: [Hover],
            SignatureInformation: [signatureInformation]
        })
    }

    /**
    * @param {vscode.TextLine} line
    * @param {vscode.CompletionItem} Completion
    * @param {vscode.Hover} Hover
    * @param {vscode.signatureInformation} signatureInformation
    * @param {vscode.InlineCompletionItem} InLine
    */
    ToSIDE(side, kind, Completion, Hover, signatureInformation, InLine = undefined) {
        if (side == "shared") {
            if (Completion != undefined) {
                this.SharedCompletion.push(Completion)
                this.ClientCompletion.push(Completion)
                this.ServerCompletion.push(Completion)
            }
            if (Hover != undefined) {
                this.SharedHover.push(Hover)
                this.ClientHover.push(Hover)
                this.ServerHover.push(Hover)
            }
            if (signatureInformation != undefined) {
                this.SharedSignature.push(signatureInformation)
                this.ClientSignature.push(signatureInformation)
                this.ServerSignature.push(signatureInformation)
            }
            if (InLine != undefined) {
                this.SharedInLine.push(Completion)
                this.ClientInLine.push(Completion)
                this.ServerInLine.push(Completion)
            }
        }
        else if (side == "server") {
            if (kind == vscode.CompletionItemKind.Event) {
                Completion.label = Completion.label;
                Completion.insertText = "\"" + Completion.label + "\""
                this.AddEventHandlerEventServer.push(Completion)
                this.ServerHover.push(Hover)
            }
            else {
                if (Completion != undefined) {
                    this.SharedCompletion.push(Completion)
                    this.ServerCompletion.push(Completion)
                }
                if (Hover != undefined) {
                    this.SharedHover.push(Hover)
                    this.ServerHover.push(Hover)
                }
                if (signatureInformation != undefined) {
                    this.SharedSignature.push(signatureInformation)
                    this.ServerSignature.push(signatureInformation)
                }
                if (InLine != undefined) {
                    this.SharedInLine.push(InLine)
                    this.ServerInLine.push(InLine)
                }
            }
        }
        else if (side == "client") {
            if (kind == vscode.CompletionItemKind.Event) {
                Completion.label = "\"" + Completion.label + "\"";
                this.AddEventHandlerEventClient.push(Completion)
                this.ClientHover.push(Hover)
            }
            else {
                if (Completion != undefined) {
                    this.SharedCompletion.push(Completion)
                    this.ClientCompletion.push(Completion)
                }
                if (Hover != undefined) {
                    this.SharedHover.push(Hover)
                    this.ClientHover.push(Hover)
                }
                if (signatureInformation != undefined) {
                    this.SharedSignature.push(signatureInformation)
                    this.ClientSignature.push(signatureInformation)
                }
                if (InLine != undefined) {
                    this.SharedInLine.push(InLine)
                    this.ClientInLine.push(InLine)
                }
            }
        }
    }

    GetHead(obj, kind) {
        let code = "";
        let params = "";
        let bracket = true;
        if (kind == vscode.CompletionItemKind.Function || kind == vscode.CompletionItemKind.Method) {
            code += "function " + obj.name;
            params = obj;
        }
        else if (kind == vscode.CompletionItemKind.Class) {
            if (obj.definition.static)
                code += "static "
            code += "class " + obj.definition.name;
            params = obj.constructors[0];
        }
        else if (kind == vscode.CompletionItemKind.Property) {
            code += "properties " + obj.name;
            params = obj;
            bracket = false;
        }
        else if (kind == vscode.CompletionItemKind.Variable) {
            code += "static " + obj.name;
            params = obj;
            bracket = false;
        }
        else if (kind == vscode.CompletionItemKind.Event) {
            code += "event " + obj.name;
            params = obj;
        }
        code += this.GetHeadParams(params, bracket);
        if (kind == vscode.CompletionItemKind.Class) {
            code += this.GetExtends(obj);
        }
        return code;
    }

    GetHeadParams(obj, bracket = true) {
        let code = "";
        if (bracket) {
            code += "(";
            if (obj == undefined) {
                code += "): void";// only class without params
                return code;
            }
            if (obj.params != undefined) {
                if (obj.params.length != 0) {
                    for (let i = 0; i < obj.params.length; i++) {
                        code += obj.params[i].name;
                        if (obj.params[i].default != null)
                            code += "? = " + obj.params[i].default;
                        code += ": " + obj.params[i].type;
                        if (i < obj.params.length - 1)
                            code += ", ";
                    }
                }
            }
            code += ")";
        }
        code += this.GetReturnsHead(obj.returns);
        return code;
    }

    GetReturnsHead(returns) {
        if (returns != null) {
            return ": " + returns.type;
        }
        else {
            return ": void";
        }
    }

    GetExtends(element) {
        let s = ""
        if (element.definition.extends != null) {
            s = " extends " + /\[(\w+)\]/.exec(element.definition.extends)[1];
        }
        return s;
    }

    GetDocumentation(obj, kind) {
        let definition = "";
        let constructors = "";
        let documentation = "";
        if (kind == vscode.CompletionItemKind.Function || kind == vscode.CompletionItemKind.Method || kind == vscode.CompletionItemKind.Property || kind == vscode.CompletionItemKind.Variable || kind == vscode.CompletionItemKind.Event) {
            definition = obj;
            constructors = obj;
        }
        else if (kind == vscode.CompletionItemKind.Class) {
            definition = obj.definition;
            constructors = obj.constructors[0];
        }
        documentation += this.GetDeprected(definition, kind);
        documentation += this.FixURL(definition.description);
        documentation += this.GetNotes(definition);
        documentation += this.GetDocumentationParams(constructors);
        documentation += this.GetDocumentationReturns(definition.returns)
        return documentation;
    }

    GetDeprected(element, kind) {
        let s = ""
        if (element.deprecated != null) {
            element.kind = kind;
            this.Deprected.push(element);
            s = "***Deprecated since version: " + element.deprecated + "***" + "\n\n"
        }
        return s;
    }

    GetNotes(definition) {
        let temp = "";
        if (definition.notes != null) {
            for (const element of definition.notes) {
                temp += this.FixURL(element)
            }
        }
        return temp;
    }

    GetDocumentationParams(obj) {
        let documentation = "";
        if (obj != null && obj.params != null) {
            for (const element of obj.params) {
                documentation += "@param " + "`" + element.name + "` " + this.FixURL(element.description);
            }
        }
        return documentation;
    }

    GetDocumentationReturns(returns) {
        if (returns != null) {
            return "@return " + this.FixURL(returns.description);
        }
        return "";
    }

    FixURL(str) {
        if (str) {
            return str.replace(/\.*\/\.*\/\.*\/*/g, "https://gothicmultiplayerteam.gitlab.io/docs/" + this.version2 + "/script-reference/") + "\n\n"
        }
        return "";
    }

    GetCompletion(name, head, documentation = null, kind = vscode.CompletionItemKind.Keyword, deprecated) {
        let Completion = new vscode.CompletionItem(name, kind);
        Completion.documentation = new vscode.MarkdownString();
        Completion.documentation.appendCodeblock(head, "typescript")
        Completion.documentation.appendMarkdown(documentation)
        Completion.documentation.supportHtml = true;
        if (deprecated)
            Completion.tags = [vscode.CompletionItemTag.Deprecated]
        return Completion;
    }

    GetHover(name, head, documentation = null) {
        // const documentation2 = new vscode.MarkdownString(`<span style='color:#f00;background-color:#fff;'><strong>AAAAAAAAAA</strong></span>`);
        // documentation2.supportHtml = true;
        // var Hover = new vscode.Hover(documentation2);
        // Hover.name = name;
        // return (Hover);
        let doc = new vscode.MarkdownString();
        doc.appendCodeblock(head, "typescript")
        doc.appendMarkdown(documentation)
        let Hover = new vscode.Hover(doc);
        Hover.name = name;
        return Hover;
    }

    GetSignature(element, name, head, description, kind) {
        let signatureInformation = new vscode.SignatureInformation(head, new vscode.MarkdownString(this.FixURL(description)))
        signatureInformation.name = name;
        signatureInformation.parameters = this.GetParameterInformation(element, kind)
        return signatureInformation;
    }

    GetParameterInformation(element, kind) {
        let tab = [];
        if (kind == vscode.CompletionItemKind.Class) {
            if (element.constructors.length != 0) {
                element = element.constructors[0];
            }
            else return tab;
        }
        else if (kind == vscode.CompletionItemKind.Property || kind == vscode.CompletionItemKind.Variable) { return tab }
        for (const element2 of element.params) {
            let temp = new vscode.ParameterInformation(element2.name, this.FixURL(element2.description))
            temp.type = element2.type;
            tab.push(temp);
        }
        return tab;
    }
}

module.exports = {
    APIparser
}