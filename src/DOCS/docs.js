const vscode = require('vscode');
const fetch = require('cross-fetch');
const APIparser = require('./APIparser')
const data = require('../data');
const deepmerge = require('../deepmerge');

async function Fetch() {
    const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
    let config = Object.assign(configuration.inspect('API').defaultValue, configuration.inspect('API').globalValue)
    var result = Object.keys(config).map((key) => [config[key]][0]);
    let url = configuration.get('APIVersion') == "prerelease" ? data.urldev : data.urlstable
    try {
        let temp = await FETCHAPI(url)
        for (let i = 0; i < result.length; i++) {
            if (result[i].indexOf("https://gitlab.com/") != -1) {
                temp = deepmerge(await FETCHFromGitlab(result[i]), temp);
            } else {
                temp = deepmerge(await FETCHAPI(result[i]), temp);
            }
        }
        temp = deepmerge(require('../DOCS/apiSquirrel.json'), temp);
        data.docs = new APIparser.APIparser(temp)
        if (configuration.get('IncludeDeprecated')) {
            data.docsDeprected = deepmerge(data.docs.Deprected, new APIparser.APIparser(require('../DOCS/deprected.json')).Deprected)
        }
    } catch (err) {
        console.error(err)
        data.docs = new APIparser.APIparser(require('../DOCS/api.json'));
    }
}
async function FETCHAPI(url) {
    const response = await fetch(url);
    if (response.status >= 400) { throw new Error("Bad response from server"); }
    return await response.json();
}

async function FETCHFromGitlab(url) {
    let reg = /https:\/\/gitlab\.com\/(.*)/
    let reg2 = /jobs\/(.*)\/artifacts/
    let match = reg.exec(url)
    if (match[1]) {
        url = "https://gitlab.com/api/v4/projects/" + match[1].replace(new RegExp("\/", 'g'), "%2F")
        let temp2s = await FETCHAPI(url + "/releases")
        for (let i = 0; i < temp2s[0].assets.links.length; i++) {
            if (temp2s[0].assets.links[i].name.lastIndexOf("_api") != -1) {
                let match = reg2.exec(temp2s[0].assets.links[i].url)
                let id = match[1]
                return await FETCHAPI(url + "/jobs/" + id + "/artifacts/api.json")
            }
        }
    }
    return undefined;
}

module.exports = {
    Fetch
}