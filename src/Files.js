const vscode = require('vscode');
const path = require('path');
const fs = require("fs");
const utility = require('./utility');
const unused = require('./Items/unusedFileDiagnostics');
const nonExist = require('./Items/nonExistFileDiagnostic');
const duplicate = require('./Items/duplicateFileDiagnostics');
const provideCompletionItemsDATA = require('./providers/provideCompletionItemsDATA');

const reg = /<(.*)\ssrc\s*=\s*"([\w\/\.\-]*)"\s*(?:type\s*=\s*"(\w*)")*/g;

let tab = [{
    fullPath: path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "config.xml"),
    type: undefined,
    range: undefined,
    fileExists: fs.existsSync(path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "config.xml")),
    inConfig: true,
    tag: undefined,
}];

function CheckIsInConfig(filename) {
    for (const element of tab) {
        if (element.fullPath == filename) return true;
    }
    return false
}

function FindElement(filename) {
    for (const element of tab) {
        if (element.fullPath == filename) return element;
    }
    return undefined
}

function CheckIsInConfigFull(element2) {
    for (const element of tab) {
        if (element.range != undefined && element2.range != undefined) {
            if (element.fullPath == element2.fullPath && utility.deepEqual(element.range, element2.range) && element.inConfig == element2.inConfig) {
                return true;
            }
        }
    }
    return false
}

function File() {
    var files = [...utility.searchRecursive(vscode.workspace.workspaceFolders[0].uri.fsPath, '.xml'), ...utility.searchRecursive(vscode.workspace.workspaceFolders[0].uri.fsPath, '.nut')];
    for (const element of files) {
        if (!CheckIsInConfig(element)) {
            tab.push({
                fullPath: element,
                type: undefined,
                range: undefined,
                fileExists: true,
                inConfig: false,
                tag: undefined
            })
            if (path.basename(element) != "bans.xml") {
                unused.Set(element);
            }
        }
    }
}
/**
* @param {vscode.TextDocument} document
*/
function Parse(document = undefined) {
    unused.Clear();
    nonExist.Clear();
    tab = [{
        fullPath: path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "config.xml"),
        type: undefined,
        range: undefined,
        fileExists: fs.existsSync(path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "config.xml")),
        inConfig: true,
        tag: undefined
    }];
    if (tab[0].fileExists) {
        for (var i = 0; i < tab.length; i++) {
            if (tab[i].fileExists && path.extname(tab[i].fullPath) === '.xml') {
                if (document != undefined && tab[i].fullPath == document.fileName) {
                    tab = [...tab, ...ParseFile(tab[i].fullPath, document.getText())];
                } else {
                    tab = [...tab, ...ParseFile(tab[i].fullPath)];
                }
            }
        }
    }
    File();
    FindDuplicate();
}

function FindDuplicate() {
    duplicate.Clear();
    let licznik = [];
    for (var i = 0; i < tab.length; i++) {
        licznik = [tab[i]];
        for (var j = 0; j < tab.length; j++) {
            if (tab[i].fullPath == tab[j].fullPath && i > j) {
                licznik.push(tab[j]);
            }
        }
        if (licznik.length > 1) {
            for (const element of licznik) {
                duplicate.Set(element.inConfig, element.range, element.fullPath)
            }
        }
    }
    return false
}

function ParseFile(filename, text = undefined) {
    if (text == undefined)
        text = fs.readFileSync(filename).toString();
    let configPath = [];
    let match;
    while (match = reg.exec(text)) {
        let matchLineNumber = utility.matchLineNumber(match);
        let range = new vscode.Range(matchLineNumber.line - 1, matchLineNumber.character - 1, matchLineNumber.line - 1, matchLineNumber.character + match[0].length - 1);
        let pathtemp = path.join(path.dirname(filename), match[2]);
        let fileExists = fs.existsSync(pathtemp);
        let element = {
            fullPath: pathtemp,
            type: match[3],
            fileExists: fileExists,
            inConfig: filename,
            range: range,
            tag: match[1]
        }
        if (!CheckIsInConfigFull(element)) {
            configPath.push(element)
        }
        if (!fileExists) {
            nonExist.Set(filename, range, pathtemp);
        }
    }
    return configPath;
}


let timeout = undefined;
function init(context) {
    Parse();
    provideCompletionItemsDATA.init();
    var watchers = [];
    var watcherXML = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(vscode.workspace.workspaceFolders[0], '**/*.xml'));
    var watcherNUT = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(vscode.workspace.workspaceFolders[0], '**/*.nut'));
    watcherXML.ignoreChangeEvents = true;
    watcherNUT.ignoreChangeEvents = true;
    watchers.push(watcherXML);
    watchers.push(watcherNUT);
    context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(e => {
        if (e.document.languageId === "xml") {
            if (timeout) {
                clearTimeout(timeout);
                timeout = undefined;
            }
            timeout = setTimeout(function () {
                Parse(e.document);
            }, 200)
        }
    }));

    context.subscriptions.push(vscode.workspace.onDidRenameFiles(e => {
        for (let z = 0; z < e.files.length; z++) {
            let oldUri = e.files[z].oldUri.fsPath;
            let newUri = e.files[z].newUri.fsPath;
            for (let i = 0; i < tab.length; i++) {
                if (tab[i].fullPath.includes(oldUri)) {
                    if (tab[i].inConfig == false) {
                        unused.RemoveItem({ fullPath: oldUri })
                    }
                    else if (tab[i].inConfig != false) {
                        let str = "";
                        if (path.extname(newUri) == "") {
                            str = "<" + tab[i].tag + " " + "src=\"" + tab[i].fullPath.replace(oldUri, newUri).replace(path.dirname(tab[i].inConfig) + path.sep, "").replace(/\\/g, "\/") + "\""
                        } else {
                            str = "<" + tab[i].tag + " " + "src=\"" + newUri.replace(path.dirname(tab[i].inConfig) + path.sep, "").replace(/\\/g, "\/") + "\""
                        }
                        if (tab[i].type) {
                            str += " type=\"" + tab[i].type + "\""
                        }
                        let txt = fs.readFileSync(tab[i].inConfig).toString();
                        let replace = function (txt, replacement, start, length) {
                            return txt.substr(0, start) + replacement + txt.substr(start + length);
                        }
                        let start = utility.PositionToIndex(txt, tab[i].range.start);
                        let end = utility.PositionToIndex(txt, tab[i].range.end);
                        let length = end - start;
                        fs.writeFileSync(tab[i].inConfig, replace(txt, str, start, length))
                        // vscode.window.showInformationMessage("The file has been renamed in config. Do you want to see the changed file?", "Yes", "No").then(function (output) {
                        //     if (output == "Yes") {
                        //         vscode.window.showTextDocument(vscode.Uri.file(tab[i].inConfig));
                        //     }
                        // })
                    }
                }
            }
            if (path.extname(oldUri) == ".xml") {
                Parse();
            }
        }
    }));
    watcherXML.onDidCreate((event) => {
        for (let i = 0; i < tab.length; i++) {
            if (tab[i].fullPath == event.fsPath) {
                tab[i].fileExists = true;
                let temp = ParseFile(event.fsPath);
                for (let k = 0; k < temp.length; k++) {
                    for (let x = 0; x < tab.length; x++) {
                        if (temp[k].fullPath == tab[x].fullPath) {
                            tab[x] = temp[k];
                        }
                    }
                    unused.RemoveItem(temp[k]);
                }
                let nonExisttemp = nonExist.Get();
                for (let j = 0; j < nonExisttemp.length; j++) {
                    let data = fs.readFileSync(nonExisttemp[j].fullPath).toString();
                    let index = utility.PositionToIndex(data, nonExisttemp[j].diagnostics.range.start);
                    let leng = nonExisttemp[j].diagnostics.range.end.character - nonExisttemp[j].diagnostics.range.start.character
                    let src = data.slice(index, index + leng);
                    let match;
                    while (match = reg.exec(src)) {
                        if (path.join(path.dirname(nonExisttemp[j].fullPath), match[2]) == event.fsPath) {
                            nonExist.Remove(j)
                            return;
                        }
                    }
                }
                return;
            }
        }
        unused.Set(event.fsPath);
        tab.push({
            fullPath: event.fsPath,
            type: undefined,
            fileExists: true,
            inConfig: undefined,
            range: undefined
        });
    })
    watcherXML.onDidDelete((event) => {
        for (let i = 0; i < tab.length; i++) {
            if (tab[i].inConfig == event.fsPath) {
                unused.Set(tab[i].fullPath);
                tab[i].inConfig = false;
                tab[i].range = undefined;
            }
            else if (tab[i].fullPath == event.fsPath && tab[i].inConfig != false) {
                tab[i].fileExists = false;
                nonExist.Set(tab[i].inConfig, tab[i].range, tab[i].fullPath)
            }
            else if (tab[i].inConfig == false && tab[i].fileExists == false) {
                tab.splice(i, 1);
                i--;
            }
        }
    })
    watcherNUT.onDidCreate((event) => {
        for (let i = 0; i < tab.length; i++) {
            if (tab[i].fullPath == event.fsPath) {
                tab[i].fileExists = true;
                nonExist.RemoveItem(tab[i])
                return;
            }
        }
        tab.push({
            fullPath: event.fsPath,
            type: undefined,
            range: undefined,
            fileExists: true,
            inConfig: false,
            tag: undefined
        })
        unused.Set(event.fsPath);
    })

    watcherNUT.onDidDelete((event) => {
        for (let i = 0; i < tab.length; i++) {
            if (tab[i].fullPath == event.fsPath) {
                tab[i].fileExists = false;
                unused.RemoveItem(tab[i])
                if (tab[i].inConfig && tab[i].range != undefined) {
                    nonExist.Set(tab[i].inConfig, tab[i].range, tab[i].fullPath)
                }
                else if (tab[i].inConfig == false && tab[i].fileExists == false) {
                    tab.splice(i, 1)
                    i--;
                }
            }
        }
    })
}


function GetFilesList(ext = undefined) {
    let temp = []
    for (const element of tab) {
        if (ext == undefined && element.fileExists)
            temp.push(element.fullPath)
        else {
            if (ext == path.extname(element.fullPath) && element.fileExists)
                temp.push(element.fullPath)
        }
    }
    return temp;
}
function GetFULLFilesList(ext = undefined) {
    let temp = []
    for (const element of tab) {
        if (ext == undefined && element.fileExists)
            temp.push(element)
        else {
            if (ext == path.extname(element.fullPath) && element.fileExists)
                temp.push(element)
        }
    }
    return temp;
}
function GetFileConfiguration() {
    return tab;
}

function Refresh() {
    Parse();
}

module.exports = {
    init, GetFilesList, GetFileConfiguration, Refresh, FindElement, GetFULLFilesList
}
