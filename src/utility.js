const vscode = require('vscode');
const fs = require('fs')
const path = require('path')
const md5File = require('md5-file')
const fetch = require('cross-fetch');
const data = require("./data");

function searchRecursive(dir, pattern) {
    var results = [];
    fs.readdirSync(dir).forEach(function (dirInner) {
        dirInner = path.resolve(dir, dirInner);
        var stat = fs.statSync(dirInner);
        if (stat.isDirectory()) {
            results = results.concat(searchRecursive(dirInner, pattern));
        }
        if (stat.isFile() && dirInner.endsWith(pattern)) {
            results.push(dirInner);
        }
    });
    return results;
};

function rgbToHex(rgb) {
    var hex = Number(rgb).toString(16);
    if (hex.length < 2) {
        hex = "0" + hex;
    }
    return hex;
};

function fullColorHex(r, g, b) {
    var red = rgbToHex(r);
    var green = rgbToHex(g);
    var blue = rgbToHex(b);
    return '#' + red + green + blue;
};

function GetG2OServerExecutableFile() {
    let temp = fs.readdirSync(vscode.workspace.workspaceFolders[0].uri.fsPath);
    for (const iterator of temp) {
        if (iterator.includes("G2O_Server")) {
            return path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, iterator)
        }
    }
    return undefined
};

async function getServerVersion() {
    var hash = "";
    var serverpath = GetG2OServerExecutableFile();
    if (serverpath != undefined) {
        let versionJson = ""
        hash = md5File.sync(serverpath)
        let url = data.urlverion
        try {
            versionJson = await FETCHAPI(url)
        } catch (err) {
            console.error(err)
            versionJson = require('./version.json')
        }
        for (const element of versionJson) {
            if (element.MD5.Linux_ARM_x86 == hash || element.MD5.Linux_ARM_x64 == hash || element.MD5.Linux_x64 == hash || element.MD5.Windows_x86 == hash || element.MD5.Windows_x64 == hash)
                return element.name;
        }
    }
    return undefined;
};

async function FETCHAPI(url) {
    const response = await fetch(url);
    if (response.status >= 400) { throw new Error("Bad response from server"); }
    return await response.json();
}

function getALLServerVersion() {
    var name = [];
    versionJson = require('./version.json')
    for (const element of versionJson) {
        name.push(element.name);
    }
    return name;
};

function matchLineNumber(m) {
    if (!m) return -1;
    let line = 1;
    let character = 1;
    for (let i = 0; i < m.index; i++) {
        character++;
        if (m.input[i] == '\n') {
            character = 1;
            line++;
        }
    }
    return { line: line, character: character };
}
function indexLineNumber(index, txt) {
    if (typeof index != "number") return -1;
    let line = 1;
    let character = 1;
    for (let i = 0; i < index; i++) {
        character++;
        if (txt[i] == '\n') {
            character = 1;
            line++;
        }
    }
    return { line: line, character: character };
}

function PositionToIndex(str, position) {
    let line = 0;
    let character = 0;
    let index = 0;
    for (let i = 0; i < str.length; i++) {
        index++;
        character++;
        if (str[i] == '\n') {
            character = 0;
            line++;
        }
        if (position.line == line && position.character == character) {
            return index;
        }

    }
    return -1;
}

function deepEqual(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
        return false;
    }
    for (const key of keys1) {
        const val1 = object1[key];
        const val2 = object2[key];
        const areObjects = isObject(val1) && isObject(val2);
        if (areObjects && !deepEqual(val1, val2) || !areObjects && val1 !== val2) {
            return false;
        }
    }
    return true;
}
function isObject(object) {
    return object != null && typeof object === 'object';
}

module.exports = { searchRecursive, rgbToHex, fullColorHex, getServerVersion, getALLServerVersion, matchLineNumber, PositionToIndex, GetG2OServerExecutableFile, indexLineNumber, deepEqual }