const docs = require('./DOCS/docs') // get data
const vscode = require('vscode')
const data = require('./data')
const provideXMLCompletionItems = require('./providers/provideXMLCompletionItems')
const provideSignatureHelp = require('./providers/provideSignatureHelp')
const provideCompletionItems = require('./providers/provideCompletionItems')
const provideFuncCompletionItems = require('./providers/provideFuncCompletionItems')
const provideHover = require('./providers/provideHover')
const provideHoverXML = require('./providers/provideHoverXML')
const provideColor = require('./providers/provideColor')
const ClientServerDiagnostic = require('./providers/ClientServerDiagnostic')
const DeprecatedDiagnostic = require('./providers/DeprecatedDiagnostic')
const EventDiagnostic = require('./providers/EventDiagnostic')
const provideInlayHints = require('./providers/provideInlayHints')
const provideInlineCompletionItems = require('./providers/provideInlineCompletionItems')
const provideDocumentLink = require('./providers/provideDocumentLink')
const ClientServerSharedDecorator = require('./providers/ClientServerSharedDecorator');
const provideCompletionItemsDATA = require('./providers/provideCompletionItemsDATA');
const statusBar = require('./statusbar')
const file = require('./Files')
const F5run = require('./F5run')
const AddUnusedFile = require('./AddUnusedFile')
const ShowFileInConfig = require('./ShowFileInConfig')

let activeEditor = vscode.window.activeTextEditor
let timeout = undefined;
/**
* @param {vscode.ExtensionContext} context
*/
async function activate(context) {
    // var fs = require('fs');
    // var path = require('path');
    // let p = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "data.xml");
    // let text = fs.readFileSync(p).toString();
    // const xmlparse = require("fast-xml-parser");
    // const parser = new xmlparse.XMLParser();
    // let jObj = parser.parse(text).data;

    // console.log(jObj)
    // console.log(jObj.items)
    // console.log(jObj.items.item[0].instance)
    // const sqparser = require("vscode-squirrel_parser")
    // console.log(sqparser.Parse(p));
    // let text = fs.readFileSync(p).toString();
    // let temp = new xmldoc.XmlDocument(fs.readFileSync(p).toString())
    // console.log(temp);
    // vscode.languages.registerCompletionItemProvider('squirrel', {
    //     provideCompletionItems(document, position, token, context) {
    //         let t = []
    //         console.log( file.GetFULLFilesList(".xml"))
    //         for (let i = 0; i < jObj.items.item.length; i++) {
    //             let completion = new vscode.CompletionItem(jObj.items.item[i].instance)
    //             completion.documentation  = new vscode.MarkdownString();
    //             let lines = JSON.stringify(jObj.items.item[i], null, 4)
    //             lines = lines.substring(lines.indexOf("\n") + 1);
    //             lines = lines.substring(lines.lastIndexOf("\n") + 1, -1)
    //             completion.documentation.appendCodeblock(lines, "json")
    //             t.push(completion)
    //         }
    //         return t;
    //     }
    // });

    await docs.Fetch()
    file.init(context);
    provideCompletionItemsDATA.init();
    F5run.run();
    vscode.commands.executeCommand('setContext', 'myExtension.ShowFileInConfig', true);
    const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
    context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory("gothicStarter", new F5run.InlineDebugAdapterFactory()));
    context.subscriptions.push(vscode.commands.registerCommand('vscode-gothiconline.launch', F5run.commandHandler));
    context.subscriptions.push(vscode.commands.registerCommand('resource-AddUnusedFile', (...commandArgs) => { AddUnusedFile.AddUnusedFile(commandArgs) }));
    context.subscriptions.push(vscode.commands.registerCommand('resource-ShowFileInConfig', (...commandArgs) => { ShowFileInConfig.ShowFileInConfig(commandArgs) }));
    if (configuration.get('InlayHints') != "disable") { context.subscriptions.push(vscode.languages.registerInlayHintsProvider('squirrel', new provideInlayHints.provideInlayHints())) }
    if (configuration.get('XML-Completion')) {
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('xml', new provideXMLCompletionItems.XMLprovideCompletionItems, '"'))
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('xml', new provideXMLCompletionItems.XMLprovideCompletionItems2, '<'))
        context.subscriptions.push(vscode.languages.registerCodeActionsProvider('xml', new provideXMLCompletionItems.XMLprovideCodeAction()))
    }
    if (configuration.get('SignatureHelp')) { context.subscriptions.push(vscode.languages.registerSignatureHelpProvider('squirrel', new provideSignatureHelp.provideSignatureHelp(), '(', ',')) }
    if (configuration.get('HoverDATA')) {
        context.subscriptions.push(vscode.languages.registerHoverProvider('squirrel', new provideCompletionItemsDATA.provideHover({})))
    }
    if (configuration.get('CompletionDATA')) {
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('squirrel', new provideCompletionItemsDATA.provideCompletionItems()))
    }
    if (configuration.get('Hover')) {
        context.subscriptions.push(vscode.languages.registerHoverProvider('squirrel', new provideHover.provideHover({})))
        context.subscriptions.push(vscode.languages.registerHoverProvider('xml', new provideHoverXML.provideHover({})))
    }
    if (configuration.get('Completion')) {
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('squirrel', new provideCompletionItems.provideCompletionItems()))
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('squirrel', new provideCompletionItems.provideCompletionItemsDOT(), '.'))
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('squirrel', new provideFuncCompletionItems.provideFuncCompletionItems2(), '=',))
        context.subscriptions.push(vscode.languages.registerCompletionItemProvider('squirrel', new provideFuncCompletionItems.provideFuncCompletionItems(), '(', ',', '"'))
    }
    if (configuration.get('InlineCompletion')) {
        context.subscriptions.push(vscode.languages.registerInlineCompletionItemProvider('squirrel', new provideInlineCompletionItems.provideInlineCompletionItems()))
    }
    if (configuration.get('DocumentLink')) {
        context.subscriptions.push(vscode.languages.registerDocumentLinkProvider('squirrel', new provideDocumentLink.provideDocumentLinks()))
    }
    if (configuration.get('Diagnostic')) {
        try {
            context.subscriptions.push(data.ClientServerDiagnostics)
            context.subscriptions.push(data.DeprecatedDiagnostics)
            context.subscriptions.push(data.EventDiagnostics)
            context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(e => {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = undefined;
                }
                timeout = setTimeout(function () {
                    if (e.document.languageId === "squirrel") {
                        ClientServerDiagnostic.onDidChangeTextDocument(data.ClientServerDiagnostics, e)
                        DeprecatedDiagnostic.onDidChangeTextDocument(data.DeprecatedDiagnostics, e)
                        EventDiagnostic.onDidChangeTextDocument(data.EventDiagnostics, e)
                    }
                }, 300)
            }));
            context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(e => {
                ClientServerDiagnostic.onDidChangeActiveTextEditor(data.ClientServerDiagnostics, e)
                DeprecatedDiagnostic.onDidChangeActiveTextEditor(data.DeprecatedDiagnostics, e)
                EventDiagnostic.onDidChangeActiveTextEditor(data.EventDiagnostics, e)
            }));
            context.subscriptions.push(vscode.workspace.onDidCloseTextDocument(e => {
                ClientServerDiagnostic.onDidCloseTextDocument(data.ClientServerDiagnostics, e)
                DeprecatedDiagnostic.onDidCloseTextDocument(data.DeprecatedDiagnostics, e)
                EventDiagnostic.onDidCloseTextDocument(data.EventDiagnostics, e)
            }));

        } catch (err) { }
    }
    if (configuration.get('Decorations')) {
        context.subscriptions.push(vscode.languages.registerColorProvider('squirrel', new provideColor.ColorPresentation()))
        context.subscriptions.push(statusBar.serverclient);
        context.subscriptions.push(statusBar.serverVersion);
        context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(editor => {
            activeEditor = editor;
            statusBar.updateStatusBarItem();
            if (editor) {
                ClientServerSharedDecorator.updateDecorations(activeEditor);
            }
        }, null, context.subscriptions))

        if (activeEditor) {
            ClientServerDiagnostic.onDidChangeTextDocument(data.ClientServerDiagnostics, activeEditor)
            DeprecatedDiagnostic.onDidChangeTextDocument(data.DeprecatedDiagnostics, activeEditor)
            EventDiagnostic.onDidChangeTextDocument(data.EventDiagnostics, activeEditor)
            statusBar.updateStatusBarItem();
        }
    }
    context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(event => {
        let affected = event.affectsConfiguration("gothic-online");
        if (affected) {
            vscode.commands.executeCommand("workbench.action.restartExtensionHost");
        }
    }))

    context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(event => {
        if (activeEditor && event.document === activeEditor.document) {
            ClientServerSharedDecorator.updateDecorations(activeEditor);
        }
    }, null, context.subscriptions));

    if (activeEditor) {
        ClientServerSharedDecorator.updateDecorations(activeEditor);
    }
}

function deactivate() { }

module.exports = {
    activate,
    deactivate
}