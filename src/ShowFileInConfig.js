const vscode = require('vscode')
const files = require('./Files')

function ShowFileInConfig(...commandArgs) {
    let uri = commandArgs[0][0]
    let config = files.GetFileConfiguration()

    for (const iterator of config) {
        if (iterator.fullPath === uri.fsPath) {
            if (iterator.inConfig) {
                uri = vscode.Uri.parse('file:' + iterator.inConfig);
                vscode.workspace.openTextDocument(uri).then(document => {
                    vscode.window.showTextDocument(document).then(editor => {
                        editor.selections = [new vscode.Selection(iterator.range.start, iterator.range.end)];
                        var range = new vscode.Range(iterator.range.start, iterator.range.end);
                        editor.revealRange(range);
                    });
                    return;
                });
            }
        }
    }
}

module.exports = {
    ShowFileInConfig

}