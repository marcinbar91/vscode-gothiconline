const vscode = require('vscode');
const unusedFileDiagnostics2 = vscode.languages.createDiagnosticCollection('unusedFileDiagnostics2')
const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
const utility = require('../utility')
let unusedFileDiagnostics = [];

function Get() {
    return unusedFileDiagnostics;
}

function Set(fullPath) {
    unusedFileDiagnostics.push({
        fullPath: fullPath,
        diagnostics: [new vscode.Diagnostic(new vscode.Range(0, 0, 0, 0), "unused file", vscode.DiagnosticSeverity.Warning)],
    });
    updateDiagnostics();
}

function Clear() {
    unusedFileDiagnostics2.clear();
    unusedFileDiagnostics = [];
}

function Remove(index) {
    unusedFileDiagnostics2.delete(vscode.Uri.file(unusedFileDiagnostics[index].fullPath));
    unusedFileDiagnostics.splice(index, 1)
}

function RemoveItem(item) {
    for (let i = 0; i < unusedFileDiagnostics.length; i++) {
        if (unusedFileDiagnostics[i].fullPath == item.fullPath) {
            Remove(i);
            i--;
        }
    }
    updateDiagnostics();
}

let serverFiles = utility.GetG2OServerExecutableFile();
let timeout = undefined;
function updateDiagnostics() {
    if (configuration.get('Diagnostic') && serverFiles) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = undefined;
        }
        timeout = setTimeout(function () {
            unusedFileDiagnostics2.clear();
            for (let i = 0; i < unusedFileDiagnostics.length; i++) {
                unusedFileDiagnostics2.set(vscode.Uri.file(unusedFileDiagnostics[i].fullPath), unusedFileDiagnostics[i].diagnostics);
            }
        }, 200)
    }
}

module.exports = {
    Get, Set, Remove, RemoveItem, Clear, updateDiagnostics
}
