const vscode = require('vscode');
const configuration = vscode.workspace.getConfiguration('gothic-online', undefined);
const nonExistFileDiagnostics2 = vscode.languages.createDiagnosticCollection('nonExistFileDiagnostics2');
let nonExistFileDiagnostic = [];

function Get() {
    return nonExistFileDiagnostic;
}

function Set(fullPath, range, pathDiagnostics) {
    let temp = true;
    for (let i = 0; i < nonExistFileDiagnostic.length; i++) {
        if (nonExistFileDiagnostic[i].fullPath == fullPath) {
            nonExistFileDiagnostic[i].diagnostics.push(new vscode.Diagnostic(range, "file doesn't exist", vscode.DiagnosticSeverity.Warning));
            nonExistFileDiagnostic[i].pathDiagnostics.push(pathDiagnostics);
            temp = false;
        }
    }
    if (temp) {
        nonExistFileDiagnostic.push({
            fullPath: fullPath,
            diagnostics: [new vscode.Diagnostic(range, "file doesn't exist", vscode.DiagnosticSeverity.Warning)],
            pathDiagnostics: [pathDiagnostics]
        });
    }
    updateDiagnostics()
}

function Clear() {
    nonExistFileDiagnostics2.clear();
    nonExistFileDiagnostic = [];
}

function Remove(index) {
    nonExistFileDiagnostic.splice(index, 1)
    updateDiagnostics()
}

function RemoveItem(item) {
    for (let i = 0; i < nonExistFileDiagnostic.length; i++) {
        if (nonExistFileDiagnostic[i].fullPath == item.inConfig) {
            for (let j = 0; j < nonExistFileDiagnostic[i].diagnostics.length; j++) {
                if (nonExistFileDiagnostic[i].diagnostics[j].range == item.range) {
                    nonExistFileDiagnostic[i].diagnostics.splice(j, 1)
                    j--;
                }
            }
        }
    }
    updateDiagnostics()
}

let timeout = undefined;
function updateDiagnostics() {
    if (configuration.get('Diagnostic')) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = undefined;
        }
        timeout = setTimeout(function () {
            nonExistFileDiagnostics2.clear();
            for (let i = 0; i < nonExistFileDiagnostic.length; i++) {
                nonExistFileDiagnostics2.set(vscode.Uri.file(nonExistFileDiagnostic[i].fullPath), nonExistFileDiagnostic[i].diagnostics);
            }
        }, 200)
    }
}

module.exports = {
    Get, Set, Remove, RemoveItem, Clear, updateDiagnostics
}
