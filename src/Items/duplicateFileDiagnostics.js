const vscode = require('vscode');
const configuration = vscode.workspace.getConfiguration('gothic-online', undefined);
const duplicateFileDiagnostics2 = vscode.languages.createDiagnosticCollection('duplicateFileDiagnostics2');
let duplicateFileDiagnostic = [];

function Get() {
    return duplicateFileDiagnostic;
}

function Set(fullPath, range, pathDiagnostics) {
    let temp = true;
    for (let i = 0; i < duplicateFileDiagnostic.length; i++) {
        if (duplicateFileDiagnostic[i].fullPath == fullPath) {
            duplicateFileDiagnostic[i].diagnostics.push(new vscode.Diagnostic(range, "duplicate", vscode.DiagnosticSeverity.Warning));
            duplicateFileDiagnostic[i].pathDiagnostics.push(pathDiagnostics);
            temp = false;
        }
    }
    if (temp) {
        duplicateFileDiagnostic.push({
            fullPath: fullPath,
            diagnostics: [new vscode.Diagnostic(range, "duplicate", vscode.DiagnosticSeverity.Warning)],
            pathDiagnostics: [pathDiagnostics]
        });
    }
    updateDiagnostics()
}

function Clear() {
    duplicateFileDiagnostics2.clear();
    duplicateFileDiagnostic = [];
}

function Remove(index) {
    duplicateFileDiagnostic.splice(index, 1)
    updateDiagnostics()
}

function RemoveItem(item) {
    for (let i = 0; i < duplicateFileDiagnostic.length; i++) {
        if (duplicateFileDiagnostic[i].fullPath == item.inConfig) {
            for (let j = 0; j < duplicateFileDiagnostic[i].diagnostics.length; j++) {
                if (duplicateFileDiagnostic[i].diagnostics[j].range == item.range) {
                    duplicateFileDiagnostic[i].diagnostics.splice(j, 1)
                    j--;
                }
            }
        }
    }
    updateDiagnostics()
}

let timeout = undefined;
function updateDiagnostics() {
    if (configuration.get('Diagnostic')) {
        if (timeout) {
            clearTimeout(timeout);
            timeout = undefined;
        }
        timeout = setTimeout(function () {
            duplicateFileDiagnostics2.clear();
            for (let i = 0; i < duplicateFileDiagnostic.length; i++) {
                duplicateFileDiagnostics2.set(vscode.Uri.file(duplicateFileDiagnostic[i].fullPath), duplicateFileDiagnostic[i].diagnostics);
            }
        }, 200)
    }
}

module.exports = {
    Get, Set, Remove, RemoveItem, Clear, updateDiagnostics
}
