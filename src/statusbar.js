const vscode = require('vscode')
const path = require('path')
const utility = require('./utility')
const files = require('./Files');
const package = require("../package.json")
const serverVersionCommand = 'config.commands.showserverVersion';
const ClientServerSharedDecorator = require('./providers/ClientServerSharedDecorator');

const unusedFileDiagnostics = require('./Items/unusedFileDiagnostics');

vscode.commands.registerCommand(serverVersionCommand, async () => {
    const target = await vscode.window.showQuickPick(utility.getALLServerVersion())
    if (target)
        serverVersion.text = target;
});
const serverclientCommand = 'config.commands.showServerClient';
vscode.commands.registerCommand(serverclientCommand, async () => {
    const target = await vscode.window.showQuickPick(["server", "client", "shared"])
    let active = vscode.window.activeTextEditor;
    let uri = undefined
    let range = undefined
    for (const iterator of files.GetFileConfiguration()) {
        if (iterator.fullPath == active.document.fileName) {
            if (iterator.inConfig != false) {
                uri = vscode.Uri.parse('file:' + iterator.inConfig);
                range = iterator.range;
            }
        }
    }
    if (target) {
        if (uri == undefined) {
            vscode.window.showInformationMessage('Error: The file does not exist in config');
            return
        }
        if (serverclient.text != target) {
            await vscode.workspace.openTextDocument(uri).then(async document => {
                const edit = new vscode.WorkspaceEdit();
                let newtext = document.getText(range).replace(/type=".*"/, "type=\"" + target + "\"")
                edit.replace(uri, range, newtext)
                return vscode.workspace.applyEdit(edit).then(async success => {
                    if (success) {
                        await document.save();
                        serverclient.text = target;
                        updateStatusBarItem()
                    } else {
                        vscode.window.showInformationMessage('Error!');
                    }
                });
            });
        }
    }
});

const apiVersionCommand = 'config.commands.showapiVersion';
vscode.commands.registerCommand(apiVersionCommand, async () => {
    const target = await vscode.window.showQuickPick(["stable", "prerelease"])
    if (target) {
        const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
        configuration.update('APIVersion', target, vscode.ConfigurationTarget.Global)
    }
});

const serverclient = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
serverclient.command = serverclientCommand;
serverclient.tooltip = package.displayName + "\nSelect server or client"
serverclient.color = "red"

const serverVersion = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 2);
serverVersion.text = "g2o: " + utility.getServerVersion();
serverVersion.command = serverVersionCommand;
serverVersion.tooltip = package.displayName + "\nSelect version"
serverVersion.color = "red"
serverVersion.show();

const apiVersion = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 1);
function updateStatusBarVersion(version) {
    apiVersion.text = "api: " + version;
    apiVersion.command = apiVersionCommand;
    apiVersion.tooltip = package.displayName + "\nGothic api version"
    apiVersion.color = "red"
    apiVersion.show();
}

function updateStatusBarItem() {
    var document = vscode.window.activeTextEditor.document
    if (path.extname(document.fileName) === '.nut') {
        files.Refresh();
        let tab = files.GetFileConfiguration()
        let type = undefined
        for (let i = 0; i < tab.length; i++) {
            if (tab[i].fullPath == document.fileName) {
                type = tab[i].type;
            }
        }
        serverclient.text = type;
        serverclient.show()
    } else {
        serverclient.hide()
    }
    unusedFileDiagnostics.updateDiagnostics()
    ClientServerSharedDecorator.updateDecorations(vscode.window.activeTextEditor);
}

module.exports = {
    updateStatusBarItem,
    serverclient,
    serverVersion,
    apiVersion,
    updateStatusBarVersion
}