const vscode = require('vscode')
const path = require('path')
const files = require('./Files')
const utility = require('./utility');

function AddUnusedFile(...commandArgs) {
    let uri = commandArgs[0][0]
    let fsPath = uri.fsPath
    let config = files.GetFileConfiguration()
    let xmlfiles = []
    let filepath = undefined
    for (const iterator of config) {
        if (path.extname(iterator.fullPath) == ".xml" && iterator.inConfig != false && (iterator.tag == "import" || iterator.tag == undefined)) {
            xmlfiles.push(iterator.fullPath)
        }
        if (iterator.fullPath == fsPath) {
            if (iterator.inConfig == false) {
                filepath = iterator.fullPath

            }
        }
    }
    if (filepath != undefined) {
        let xmlfile = xmlfiles[0]
        let replaced = filepath.replace(path.dirname(xmlfiles[0]), "").substring(1)
        for (const iterator of xmlfiles) {
            let replacedTemp = filepath.replace(path.dirname(iterator), "")
            if (filepath.length > replacedTemp.length) {
                if (replacedTemp.length < replaced.length) {
                    xmlfile = iterator;
                    replaced = replacedTemp.substring(1);
                }
            }
        }
        uri = vscode.Uri.parse('file:' + xmlfile);
        vscode.workspace.openTextDocument(uri).then(document => {
            const edit = new vscode.WorkspaceEdit();
            let newtext = document.getText()
            let type = "shared"
            let pos = utility.indexLineNumber(newtext.lastIndexOf("</server>"), newtext)
            if (filepath.includes('server-scripts')) {
                type = 'server'
            }
            else if (filepath.includes('client-scripts')) {
                type = 'client'
            }
            replaced = replaced.replace(/\\/g, "\/")
            edit.insert(uri, new vscode.Position(pos.line - 1, pos.character - 1), "\t<script src=\"" + replaced + "\" type=\"" + type + "\"/>\n")
            return vscode.workspace.applyEdit(edit).then(success => {
                if (success) {
                    vscode.workspace.saveAll(false)
                    //vscode.window.showTextDocument(document);
                } else {
                    vscode.window.showInformationMessage('Error!');
                }
            });
        });
    }
}

module.exports = {
    AddUnusedFile
}