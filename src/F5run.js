const vscode = require('vscode');
const path = require('path')
const fs = require('fs')
const regedit = require('registry-js');
const os = require('os');
const utility = require('./utility')

let launchJson = {
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Gothic Starter",
            "type": "gothicStarter",
            "request": "launch",
            "vscCommand": "vscode-gothiconline.launch"
        }
    ]
}
//https://vshaxe.github.io/vscode-extern/vscode/Pseudoterminal.html
//https://code.visualstudio.com/api/references/vscode-api#Pseudoterminal
const commandHandler = () => {
    if (os.type() === "Windows_NT") {
        const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
        let terminal = vscode.window.createTerminal("gothicStarter");
        let server = path.basename(utility.GetG2OServerExecutableFile());
        if (server) {
            terminal.sendText("TASKKILL /F /IM " + path.basename(server) + " /IM Gothic2.exe /IM G2O_URLProtocol.exe");
            // terminal.sendText("start " + server.replace(" ", "\" \""))
            terminal.sendText("start " + server);
            let key = regedit.enumerateValuesSafe(regedit.HKEY.HKEY_CLASSES_ROOT, 'g2o\\shell\\open\\command')
            if (key.length == 0) {
                console.error("The registery key could not be found");
                return;
            } else {
                if (!fs.existsSync(key[0].data.replace(" \"%1\"", ""))) {
                    console.error("G2O_URLProtocol.exe file doesn't exists. Try to reinstall \"Gothic 2 Online addon\"");
                    return;
                }
            }
            terminal.sendText("timeout " + configuration.get('timeout'));
            terminal.sendText("start g2o://" + configuration.get('LaunchIP') + ":" + configuration.get('LaunchPort') + "?nickname=" + configuration.get('LaunchPlayerName'));
        } else if (os.type() === "Linux") {
            terminal.sendText("killall -r " + server);
            terminal.sendText("./" + server);
        }
        else {
            console.error("Unsupported OS")
        }
        //terminal.show(); // for debug
    }
};

function run() {
    const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
    let vscodeFolder = path.join(vscode.workspace.workspaceFolders[0].uri.fsPath, '.vscode');
    if (!fs.existsSync(vscodeFolder) && configuration.get('AutoLaunch')) {
        let answer1 = "Yes";
        let answer2 = "No";
        let answer3 = "No and don't ask";
        vscode.window.showInformationMessage("Do you want create auto launch server and game?", answer1, answer2, answer3).then(function (output) {
            if (output == answer1) {
                let launchJsonPath = path.join(vscodeFolder, 'launch.json');
                fs.mkdirSync(vscodeFolder);
                fs.writeFileSync(launchJsonPath, JSON.stringify(launchJson, function (key, value) { return value }, "\t"));
            }
            if (output == answer3) {
                configuration.update('AutoLaunch', false, vscode.ConfigurationTarget.Global)
            }
        })
    }
}

const debugadapter = require("@vscode/debugadapter");
let ownTerminal;
function getTerminal(configuration) {

    if (configuration.hasOwnProperty("terminalIndex") &&
        Number.isInteger(configuration.terminalIndex) &&
        configuration.terminalIndex >= 0 &&
        configuration.terminalIndex < vscode.window.terminals.length) {
        return vscode.window.terminals[configuration.terminalIndex];
    }
    if (!ownTerminal || ownTerminal.exitStatus) {
        let name = "Gothic Starter";
        if (configuration.hasOwnProperty("terminalName")) {
            name = String(configuration.terminalName);
        }
        ownTerminal = vscode.window.createTerminal(name);
    }
    return ownTerminal;
}

class InlineDebugAdapterFactory {
    createDebugAdapterDescriptor(session) {
        const command = session.configuration.hasOwnProperty("command") && session.configuration.command;
        const vscCommand = session.configuration.hasOwnProperty("vscCommand") && session.configuration.vscCommand;
        if (!command && !vscCommand) {
            vscode.window.showWarningMessage(`No command or vscCommand found in launch configuration "${session.configuration.name}". Add one like "command": "echo Hello" or "vscCommand": "editor.action.formatDocument" to your launch.json.`);
        }
        else {
            if (vscCommand) {
                vscode.commands.executeCommand(vscCommand);
            }
            if (command) {
                const terminal = getTerminal(session.configuration);
                if (!session.configuration.hasOwnProperty("showTerminal") || session.configuration.showTerminal) {
                    terminal.show();
                }
                terminal.sendText(String(command));
            }
        }
        return new vscode.DebugAdapterInlineImplementation(new DummyDebugSession());
    }
}
class DummyDebugSession extends debugadapter.DebugSession {
    initializeRequest() {
        this.sendEvent(new debugadapter.TerminatedEvent());
    }
}


// function ShowDialog() {
//     vscode.window.showOpenDialog({
//         canSelectMany: false,
//         title: 'Select G2O_URLProtocol.exe',
//         canSelectFiles: true,
//         canSelectFolders: false,
//         filters: {
//             'Executable files': ['exe'],
//         }
//     }).then(fileUri => {
//         if (fileUri && fileUri[0]) {

//         }
//     });
// }

module.exports = {
    InlineDebugAdapterFactory,
    commandHandler,
    run
}