const vscode = require('vscode');
const path = require('path');
const utility = require('../utility');
const files = require('../Files');
const data = require('../data');

//To-DO add support class and static class eg. Camera.setMode()
function getDiagnostic(file) {
    let path2 = file.fileName
    let text = file.getText();
    let match;
    let diagnostics = [];
    let type = files.FindElement(path2).type
    if (path.extname(path2) === '.nut') {
        if (type == "server") {
            while (match = data.DiagnosticReg.exec(text)) {
                if (isContain(data.docs.ClientCompletion, match[1]) && !isContain(data.docs.ServerCompletion, match[1])) {
                    let temp = utility.matchLineNumber(match);
                    let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                    diagnostics.push(new vscode.Diagnostic(range, "This is client function", vscode.DiagnosticSeverity.Warning));
                }
            }
        }
        else if (type == "client") {
            while (match = data.DiagnosticReg.exec(text)) {
                if (isContain(data.docs.ServerCompletion, match[1]) && !isContain(data.docs.ClientCompletion, match[1])) {
                    let temp = utility.matchLineNumber(match);
                    let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                    diagnostics.push(new vscode.Diagnostic(range, "This is server function", vscode.DiagnosticSeverity.Warning));
                }
            }
        }
        else
            return [];
    }
    return diagnostics;
}

function isContain(doc, match) {
    for (const element of doc) {
        if (match == element.label) {
            return true;
        }
    }
    return false;
}

function onDidChangeTextDocument(ClientServerDiagnostic, e) {
    ClientServerDiagnostic.delete(e.document.uri);
    ClientServerDiagnostic.set(e.document.uri, getDiagnostic(e.document));
}

function onDidChangeActiveTextEditor(ClientServerDiagnostic, e) {
    if (e) ClientServerDiagnostic.set(e.document.uri, getDiagnostic(e.document))
}

function onDidCloseTextDocument(ClientServerDiagnostic, doc) {
    ClientServerDiagnostic.delete(doc.uri)
}

module.exports = {
    onDidChangeTextDocument, onDidChangeActiveTextEditor, onDidCloseTextDocument
}

