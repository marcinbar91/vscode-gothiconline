const vscode = require('vscode');
const p = require("path");
const fs = require('fs')
const statusBar = require('../statusbar')
const item = require('../Items/Item');
const data = require('../data');

class provideFuncCompletionItems extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        const line = document.lineAt(position.line);
        let Idx = line.text.lastIndexOf('(', position.character)
        if (Idx === -1) return ([])

        return new Promise((resolve, reject) => {
            resolve(
                provideCompletionAddEventHandler(line, document, position) ||
                provideCompletionSetPlayerTalent(line, document, position) ||
                provideCompletionattackPlayer(line, document, position) ||
                provideCompletionHud(line, document, position) ||
                provideCompletionKey(line, document, position) ||
                provideCompletionLogicalKey(line, document, position) ||
                provideCompletionMouse(line, document, position) ||
                provideCompletionDamage(line, document, position) ||
                provideCompletionPlanet(line, document, position) ||
                provideCompletionHand(line, document, position) ||
                provideCompletionSkillweapon(line, document, position) ||
                provideCompletionWeaponmode(line, document, position) ||
                provideCompletionMdsID(line, document, position) ||
                provideCompletionItemsID(line, document, position) ||
                provideCompletionVob(line, document, position) ||
                provideCompletionTexture(line, document, position) ||
                provideCompletiontextSetFont(line, document, position) ||
                provideCompletionFont(line, document, position) ||
                provideCompletionPlayerInstance(line, document, position) ||
                provideCompletionPlayAni(line, document, position) ||
                provideCompletionReliability(line, document, position) ||
                provideCompletionCameraSetMode(line, document, position) ||
                provideCompletionBloodMode(line, document, position) ||
                provideCompletionHeroStatus(line, document, position) ||
                provideCompletionTraceRay(line, document, position) ||
                provideCompletionKeyDelay(line, document, position) ||
                provideCompletioTraceRay(line, document, position) ||
                provideCompletionCollisionObject(line, document, position)
            )
        });
    }
}

class provideFuncCompletionItems2 extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        const line = document.lineAt(position.line);
        let Idx = line.text.lastIndexOf('=', position.character)
        if (Idx === -1) return ([])

        return new Promise((resolve, reject) => {
            resolve(
                provideCompletionWeather(line, document, position) ||
                provideDirection(line, document, position) ||
                provideMode(line, document, position) ||
                provideTriggerListProcess(line, document, position)
            )
        });
    }
}

/**
* @param {vscode.TextLine} line
* @param {vscode.TextDocument} document
* @param {vscode.Position} position
*/
function provideCompletionAddEventHandler(line, document, position) {
    const commentIndex = document.lineAt(position.line).text.indexOf('(');
    let reg = /(addEventHandler)\s*\((\"(.*)?\".*)?\)/g
    let match;
    while (match = reg.exec(line.text)) {
        if (position.character - 1 == commentIndex) {
            if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
                if (statusBar.serverclient.text == "server") {
                    return data.docs.AddEventHandlerEventServer;
                }
                else if (statusBar.serverclient.text == "client") {
                    return data.docs.AddEventHandlerEventClient;
                }
                else return [...data.docs.AddEventHandlerEventClient, ...data.docs.AddEventHandlerEventServer];
            }
        }
        else if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            let match1;
            let reg1 = /(local\s|static\s)?function\s*([\w.:]+)\s?\([^\)]*\)\s*{/gms
            let tab = [new vscode.CompletionItem("function")]
            let params = ""
            for (const element of data.docs.DOCS2.events) {
                if (element.name == match[3]) {
                    for (let i = 0; i < element.params.length; i++) {
                        params += element.params[i].name
                        if (i < element.params.length - 1)
                            params += ", "
                    }
                }
            }
            tab[0].insertText = new vscode.SnippetString("function (" + params + ") {\n\t$0\n}")
            tab[0].kind = vscode.CompletionItemKind.Function
            tab[0].documentation = new vscode.MarkdownString();
            tab[0].documentation.appendCodeblock("addEventHandler (" + match[3] + ", function (" + params + ") { });", "squirrel")
            while (match1 = reg1.exec(document.getText())) {
                tab.push(new vscode.CompletionItem(match1[2], vscode.CompletionItemKind.Function));
            }
            return tab;
        }
    }
    return null;
}

function provideCompletionReliability(line, document, position) {
    const range = document.getWordRangeAtPosition(position.with(position.line, position.character - 1));
    const word = document.lineAt(position.line).text.slice(0, range.end.character).match(/[\w.]+$/g)[0];
    var variable = new item.variable(document);
    if (word.indexOf('.') != -1) {
        for (const element2 of variable.list) {
            if (word.split('.')[0] == element2.name) {
                if (element2.value == "Packet") {
                    if ("client" == statusbar.serverclient.text) {
                        return GetConstants(data.docs.Constants, "Reliability");
                    }
                    else if ("server" == statusbar.serverclient.text) {
                        if (line.indexOf(',') != -1) {
                            return GetConstants(data.docs.Constants, "Reliability");
                        }
                    }
                }
            }
        }
    }
    return null;
}

function provideCompletionTraceRay(line, document, position) {
    let match;
    let reg = /.*(?:(traceRayFirstHit)|(traceRayNearestHit))\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 2) < position.character && getPosition(match[0], ',', 3) > position.character) {
            return GetConstants(data.docs.Constants, "TraceRay");
        }
    }
    return null;
}

function provideCompletionSetPlayerTalent(line, document, position) {
    let match;
    let reg = /(setPlayerTalent|getPlayerTalent)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            return GetConstants(data.docs.Constants, "Talent");
        }
    }
    return null;
}

function provideCompletionattackPlayer(line, document, position) {
    let match;
    let reg = /(attackPlayer)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 2) < position.character && getPosition(match[0], ',', 3) > position.character) {
            return GetConstants(data.docs.Constants, "Attack");
        }
    }
    return null;
}

function provideCompletionCollisionObject(line, document, position) {
    let match;
    let reg = /(setCollisionClass)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "CollisionObject");
        }
    }
    return null;
}

function provideCompletionHeroStatus(line, document, position) {
    let match;
    let reg = /(setHeroStatus)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
           
            return GetConstants(data.docs.Constants, "Hero Status");
        }
    }
    return null;
}

function provideCompletionKeyDelay(line, document, position) {
    let match;
    let reg = /(getKeyDelayFirst|getKeyDelayRate)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "Key delay");
        }
    }
    return null;
}

function provideCompletioTraceRay(line, document, position) {
    let match;
    let reg = /(traceRayFirstHit|traceRayNearestHit)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 2) < position.character && getPosition(match[0], ',', 3) > position.character) {
            return GetConstants(data.docs.Constants, "TraceRay");
        }
    }
    return null;
}

function provideCompletionHud(line, document, position) {
    let match;
    let reg = /(getBarPosition|getBarSize|setBarPosition|setBarSize|setHudMode|getHudMode)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "HUD");
        }
    }
    return null;
}

//TO-DO missing event onKey
function provideCompletionKey(line, document, position) {
    let match;
    let reg = /(getKeyLetter|isKeyPressed|isKeyDisabled|disableKey|isKeyToggled)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "Key");
        }
    }
    return null;
}

function provideCompletionLogicalKey(line, document, position) {
    let match;
    let reg = /(disableLogicalKey|isLogicalKeyDisabled|isLogicalKeyPressed|getLogicalKeyBinding|setLogicalKeyBinding)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "Logical key");
        }
    }
    return null;
}

//TO-DO missing event onMouseClick      event onMouseRelease
function provideCompletionMouse(line, document, position) {
    let match;
    let reg = /(isMouseBtnPressed)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "Mouse");
        }
    }
    return null;
}
function provideCompletionBloodMode(line, document, position) {
    let match;
    let reg = /(setBloodMode)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "BloodMode");
        }
    }
    return null;
}

function provideCompletionDamage(line, document, position) {
    let match;
    let reg = /(attackPlayerWithEffect)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 2) < position.character && getPosition(match[0], ',', 3) > position.character) {
            return GetConstants(data.docs.Constants, "Damage");
        }
    }
    reg = /(getPlayerProtection)\s*\((.*)\)/g;
    match = [];
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            return GetConstants(data.docs.Constants, "Damage");
        }
    }
    return null;
}

function provideCompletionPlanet(line, document, position) {
    let match;
    let reg = /(Sky.setPlanetColor|Sky.setPlanetSize)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 0) < position.character && getPosition(match[0], ',', 1) > position.character) {
            return GetConstants(data.docs.Constants, "Planet");
        }
    }
    return null;
}

function provideCompletionWeather(line, document, position) {
    let match;
    let reg = /(Sky.weather)\s*=/g;
    while (match = reg.exec(line.text)) {
        return GetConstants(data.docs.Constants, "Weather");
    }
    return null;
}

//TO-DO missing event onPlayerEquipHandItem     event onUseItem
function provideCompletionHand(line, document, position) {
    let match;
    let reg = /(getPlayerRing)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            return GetConstants(data.docs.Constants, "Hand");
        }
    }
    return null;
}

function provideCompletionSkillweapon(line, document, position) {
    let match;
    let reg = /(getPlayerSkillWeapon|setPlayerSkillWeapon)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            return GetConstants(data.docs.Constants, "Skill weapon");
        }
    }
    return null;
}


function provideCompletionWeaponmode(line, document, position) {
    let match;
    let reg = /(setPlayerWeaponMode|drawWeapon)\s*\((.*)\)/g;
    while (match = reg.exec(line.text)) {
        if (getPosition(match[0], ',', 1) < position.character && getPosition(match[0], ',', 2) > position.character) {
            return GetConstants(data.docs.Constants, "Weapon mode");
        }
    }
    return null;
}

function provideDirection(line, document, position) {
    let match;
    let reg = /(.direction)\s*=\s*/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            return GetConstants(data.docs.Constants, "MobInterDirection");
        }
    }
    return null;
}

function provideMode(line, document, position) {
    let match;
    let reg = /(.mode)\s*=\s*/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            return GetConstants(data.docs.Constants, "RigidBody");
        }
    }
    return null;
}

function provideTriggerListProcess(line, document, position) {
    let match;
    let reg = /(.listProcess)\s*=\s*/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            return GetConstants(data.docs.Constants, "TriggerList Process");
        }
    }
    return null;
}

function provideCompletionCameraSetMode(line, document, position) {
    let match;
    let reg = new RegExp('(Camera.setMode)' + data.provideFuncCompletionItemsparamsRegex.source, 'g');
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let camera = ["CAMMODNORMAL", "CAMMODRUN", "CAMMODDIALOG", "CAMMODINVENTORY", "CAMMODMELEE", "CAMMODMAGIC", "CAMMODMELEEMULT", "CAMMODRANGED", "CAMMODSWIM", "CAMMODDIVE",
                "CAMMODJUMP", "CAMMODJUMPUP", "CAMMODCLIMB", "CAMMODDEATH", "CAMMODLOOK", "CAMMODLOOKBACK", "CAMMODFOCUS", "CAMMODRANGEDSHORT", "CAMMODSHOULDER", "CAMMODFIRSTPERSON",
                "CAMMODTHROW", "CAMMODMOBLADDER", "CAMMODFALL"];
            var items = [];
            for (const element of camera) {
                items.push(new vscode.CompletionItem(element));
            }
            return items;
        }
    }
    return null;
}

function provideCompletionMdsID(line, document, position) {
    var path = p.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "mds.xml");
    if (!fs.existsSync(path)) resolve([]);
    let text = fs.readFileSync(path);

    var reg = /<name>([1-2A-z\/\-._]*)<\/name>/g;
    let match;
    let mds = [];
    while (match = reg.exec(text)) {
        mds.push(new vscode.CompletionItem(match[1]));
    }
    reg = new RegExp('(Mds.id)' + data.provideFuncCompletionItemsparamsRegex.source, 'g');
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            return mds;
        }
    }
    return null;
}

function provideCompletionItemsID(line, document, position) {
    var path = p.join(vscode.workspace.workspaceFolders[0].uri.fsPath, "items.xml");
    if (!fs.existsSync(path)) return null;
    let text = fs.readFileSync(path);
    var reg = /<instance>([1-2A-z\/\-._]*)<\/instance>/g;
    let match;
    let items = [];
    while (match = reg.exec(text)) {
        items.push(new vscode.CompletionItem(match[1]));
    }
    reg = new RegExp('(Items.id)' + data.provideFuncCompletionItemsparamsRegex.source + '|(hasItem)' + data.provideFuncCompletionItemsparamsRegex.source, 'g');
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            return items;
        }
    }
    return null;
}

function provideCompletionPlayAni(line, document, position) {
    let match;
    let reg = /(playAni)\s*\(\s*.*,\s*"{1}/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let animations = ["T_DANCE", "T_DANCE_09", "T_DANCE_08", "T_DANCE_07", "T_DANCE_06", "T_DANCE_05", "T_DANCE_04", "T_DANCE_03", "T_DANCE_02", "T_DANCE_01", "S_FIRE_VICTIM", "T_WARN", "T_STAND_2", "T_GUARDSLEEP_2_STAND", "S_HANG", "T_PRACTICEMAGIC", "T_PRACTICEMAGIC2", "T_PRACTICEMAGIC3", "T_PRACTICEMAGIC4", "T_FIRESPIT_S0_2_S1", "T_JOINT_RANDOM_1", "S_PYRSHOOT", "T_1HSINSPECT", "T_MAP_STAND_2_S0", "T_SEARCH", "T_YES", "T_DONTKNOW", "S_PRAY", "T_STAND_2_PRAY", "T_PRAY_2_STAND", "T_IDOL_S0_2_S1", "T_STAND_2_FEAR_VICTIM2", "T_STAND_2_SWARM_VICTIM", "T_STAND_2_INFLATE_VICTIM", "T_STAND_2_SUCKENERGY_VICTIM", "T_SDW_S0_2_S1", "T_1HSFREE", "T_1HSFINISH", "S_HGUARD", "T_STAND_2_HGUARD", "T_HGUARD_2_STAND", "S_LGUARD", "T_STAND_2_LGUARD", "T_LGUARD_2_STAND", "S_PEE", "T_STAND_2_PEE", "T_PEE_2_STAND", "S_SIT", "T_STAND_2_SIT", "T_SIT_2_STAND", "T_IGETYOU", "T_ORE_S0_2_S0", "T_ORE_S0_2_S1", "T_MAP_STAND_2_S0", "T_SLEEP", "T_STAND_2_SLEEPGROUND", "T_SLEEPGROUND_2_STAND", "T_GETLOST", "T_GETLOST2", "S_REPAIR_S1", "T_REPAIR_S0_2_S1", "T_REPAIR_RANDOM_1", "T_STAND_2_WOUNDED", "T_STAND_2_WOUNDEDB", "T_DEAD", "T_DEADB", "T_WATCHFIGHT_OHNO", "S_WASH", "T_STAND_2_WASH", "T_WASH_2_STAND", "S_WATCHFIGHT", "T_STAND_2_WATCHFIGHT", "T_WATCHFIGHT_2_STAND", "T_FIRESPIT_S0_2_S1", "S_CELLO_S0", "T_CELLO_STAND_2_S0", "T_CELLO_S0_2_STAND", "S_HARFE_S0", "T_HARFE_STAND_2_S0", "T_HARFE_S0_2_STAND", "S_PAUKE_S0", "T_PAUKE_STAND_2_S0", "T_PAUKE_S0_2_STAND", "S_DRUMSCHEIT_S0", "T_DRUMSCHEIT_STAND_2_S0", "T_DRUMSCHEIT_S0_2_STAND", "S_DUDEL_S0", "T_DUDEL_STAND_2_S0", "T_DUDEL_S0_2_STAND", "T_POTION_RANDOM_2", "T_IDROP_2_STAND", "T_FORGETIT", "T_IGET_2_STAND", "T_TRADEITEM", "T_HERB_S0_2_S1", "T_BORINGKICK", "T_PAN_S0_2_S1", "T_FOODHUGE_RANDOM_1", "T_MEAT_RANDOM_1", "T_GRAVE_S0_2_S1", "S_BSANVIL_S1", "T_BSCOOL_S0_2_S1", "T_BSFIRE_S0_2_S1", "T_BSSHARP_S0_2_S1", "S_SLIDE", "T_STAND_2_WOUNDED", "T_WOUNDED_2_STAND", "T_PLUNDER", "T_GREETNOV", "S_RMAKER_S1", "T_WATCHFIGHT_YEAH", "T_PAN_S1_2_S0", 'T_PAN_S0_2_S1', "T_COMEOVERHERE", "T_GRAVE_STAND_2_S0", "T_GRAVE_S0_2_S1", "T_1HSINSPECT", "T_POINT", "S_BAUMSAEGE_S1", "T_POTION_RANDOM_1", "T_POTION_RANDOM_2", "T_POTION_RANDOM_3", "T_FIRESPIT_STAND_2_S0", "T_FIRESPIT_STAND_2_S1", "T_FIRESPIT_STAND_2_S2", "T_FIRESPIT_STAND_2_S3", "T_FIRESPIT_STAND_2_S4", "S_LAB_S1", "T_BSCOOL_S0_2_S1", "T_BSANVIL_S0_2_S1", "T_BSFIRE_S0_2_S1", "T_BSSHARP_S0_2_S1", "S_SNEAKL", "S_1HSNEAKL", "S_2HSNEAKL", "S_MAGSNEAKL", "S_BOWSNEAKL", "S_CBOWSNEAKL",
                "R_EYESBLINK", "S_ANGRY", "S_EYESCLOSED", "S_FRIENDLY", "S_FRIGHTENED", "S_HOSTILE", "S_NEUTRAL", "T_EAT", "T_HURT", "VISEME",];
            var items = [];
            for (const element of animations) {
                items.push(new vscode.CompletionItem(element));
            }
            return items;
        }
    }
    return null;
}

function provideCompletionTexture(line, document, position) {
    let match;
    let reg = /(Texture)\s*\(\s*.*,\s*"{1}/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let texture = ["DLG_CONVERSATION.TGA", "MENU_INGAME.TGA", "INV_SLOT_FOCUS.TGA", "INV_SLOT_HIGHLIGHTED_FOCUS.TGA", "PROGRESS.TGA", "LOG_BACK.TGA", "LOG_PAPER.TGA", "MENU_CHOICE_BACK.TGA", "MENU_SLIDER_BACK.TGA", "INV_TITEL.TGA", "LETTERS.TGA", "INV_SLOT_EQUIPPED_HIGHLIGHTED_FOCUS.TGA", "INV_SLOT_EQUIPPED_FOCUS.TGA", "CONSOLE.TGA", "FOCUS_HIGHLIGHT.TGA", "MENU_INGAME.TGA", "BAR_BACK.TGA", "BAR_HEALTH.TGA", "Dlg_Conversation.TGA", "Map_NewWorld.TGA", "SELECT.TGA", "ARROW_DOWN_ELM.TGA", "ARROW_UP_ELM.TGA", "ARROW_LEFT_ELM.TGA", "ARROW_RIGHT_ELM.TGA", "BUTTON_ELM.TGA", "ROUND_ELM.TGA", "CHECKBOX_CHECKED_ELM.TGA", "CHECKBOX_ELM.TGA", "FRAME_ELM.TGA", "SLIDER_ELM.TGA", "LABEL_ELM.TGA", "WINDOW_1_1_ELM.TGA", "LOGO_ELM.TGA", "WINDOW_16_9_ELM.TGA", "WINDOW_4_3_ELM.TGA", "WINDOW_5_4_ELM.TGA", "DLG_CONVERSATION.TGA", "MENU_INGAME.TGA", "INV_SLOT_FOCUS.TGA", "INV_SLOT_HIGHLIGHTED_FOCUS.TGA", "PROGRESS.TGA"];
            var items = [];
            for (const element of texture) {
                items.push(new vscode.CompletionItem(element));
            }
            return (items);
        }
    }
    return null;
}

function provideCompletionVob(line, document, position) {
    let match;
    let reg = new RegExp('(Vob)' + data.provideFuncCompletionItemsparamsRegex.source, 'g');
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let vob = ["NW_HARBOUR_CRATE_01.3DS", "NW_HARBOUR_CRATEPILE_01.3DS", "NW_HARBOUR_BARRELGROUP_01.3DS", "NW_HARBOUR_CANONBALLS_01.3DS", "NW_CITY_OILLAMP_01.3DS", "NW_CITY_CANDLE_01.3DS", "FIRE_COMPLETE_A0.TGA", "NW_CITY_BOOKSHELF_01.3DS", "NW_CITY_CUPBOARD_RICH_02.3DS", "NW_HARBOUR_BARRELGROUP_02.3DS", "NW_HARBOUR_FISHINGNETSMALL.3DS", "NW_HARBOUR_CANON_01.3DS", "DT_SHELF_V1.3DS", "NW_HARBOUR_FISHINGNETBIG.3DS", "NW_CITY_DECO_PICTURE_01.3DS", "DT_TABLE_V3.3DS", "NW_CITY_CUPBOARD_POOR_02.3DS", "NW_CITY_CUPBOARD_POOR_01.3DS", "NW_HARBOUR_BARREL_01.3DS", "NW_CITY_DECO_WHEEL_01.3DS", "NW_CITY_TABLE_RICH_02.3DS", "NW_CITY_DECO_WOLFHEAD_01.3DS", "NW_CITY_CUPBOARD_RICH_01.3DS", "NW_SHIP_STAIRS_NAVROOM_01.3DS", "NW_SHIP_STAIRS_AND_MASTS_01.3DS", "NW_HARBOUR_ANCHOR_01.3DS", "NW_HARBOUR_CANONBALLS_02.3DS", "BOOK_NW_CITY_CUPBOARD_01.ASC", "THRONE_NW_CITY_01.ASC", "BEDHIGH_NW_MASTER_01.ASC", "CHAIR_NW_NORMAL_01.ASC", "NW_CITY_TABLE_NORMAL_01.3DS", "STOVE_NW_CITY_01.ASC", "CAULDRON_OC.ASC", "BSSHARP_OC.MDS", "BSFIRE_OC.MDS", "BSCOOL_OC.MDS", "BSANVIL_OC.MDS", "BENCH_3_OC.ASC", "DOOR_NW_RICH_01.MDS", "LAB_PSI.ASC", "RMAKER_1.MDS", "BEDHIGH_NW_NORMAL_01.ASC", "CHESTBIG_NW_RICH_LOCKED.ASC", "ORC_SKELETON_V3.3DS", "ORC_SKELETON_V1.3DS", "OW_MISC_WALL_TORCH_01.3DS", "ZFLARE6.TGA", "NW_CITY_KNIGHT_ARMOR_01.3DS", "ORC_SKULLSONFLOOR.3DS", "NW_CITY_CARPET_03.3DS", "NW_MISC_SKELETON_04.3DS", "CHESTBIG_NW_RICH_OPEN.MDS", "DOOR_NW_DRAGONISLE_02.MDS", "NW_CITY_TABLE_PEASANT_01.3DS", "TOUCHPLATE_STONE.MDS", "TURNSWITCH_BLOCK.MDS", "ORC_SKELETON_V2.3DS", "NW_MISC_FIREPLACE_01.3DS", "OC_FIREPLACE_V5.3DS", "OC_BARREL_V1_DESTROYED.3DS", "NW_MISC_SKELETON_02.3DS", "OC_LOB_CHAIN.3DS", "NW_DRAGONISLE_TORTURE_04.3DS", "NW_DRAGONISLE_TORTURE_05.3DS", "OC_LOB_CAGE.3DS", "EVT_TPL_METALTHORNS_2X2X05M.3DS", "NW_DRAGONISLE_TORTURE_01.3DS", "NW_DRAGONISLE_TORTURE_03.3DS", "ORC_FIREPLACE_V3.3DS", "NW_CITY_KNIGHT_STATUE_01.3DS", "CHESTBIG_OCCHESTLARGELOCKED.MDS", "INNOS_NW_MISC_01.ASC", "NW_DRAGONISLE_TORTURE_02.3DS", "LEVER_1_OC.MDS", "CHESTBIG_NW_RICH_LOCKED.MDS", "CHESTBIG_NW_NORMAL_LOCKED.MDS", "DT_SKELETON_V02.3DS", "NW_CAVEWEBS_V201.3DS", "OW_NATURE_SMALLTREE_01.3DS", "OW_MUSHROOM_V2.3DS", "NW_NATURE_STONE_MANYSTONES_01.3DS", "OSTA_AXE2.3DS", "ORC_STANDING_LAMP_LARGE.3DS", "OW_CAVEWEBS_V2.3DS", "NW_MISC_SKELETON_03.3DS", "NW_MISC_GRAVESTONE_02.3DS", "BENCH_NW_OW_01.ASC", "OW_CAVEWEBS_V1.3DS", "OC_LOB_TORCHHOLDER_FILLED.3DS", "DOOR_NW_DRAGONISLE_01.MDS", "ORC_SKELETON_V4.3DS", "OC_SHELF_V4.3DS", "NW_CITY_MAP_WAR_OPEN_01.3DS", "NW_CITY_MAP_WAR_CLOSED_01.3DS", "CHESTBIG_OCCHESTMEDIUMLOCKED.MDS", "BARBQ_SCAV.MDS", "OC_GATE_KITCHEN.3DS", "BOOK_BLUE.MDS", "OC_WEAPON_WALLARMOR.3DS", "OC_TABLE_V3.3DS", "OW_NATURE_BUSH_01.3DS", "OW_NATURE_BUSH_02.3DS", "OW_MISC_BURNEDHUT_01.3DS", "DOOR_WOODEN.MDS", "BENCH_NW_CITY_01.ASC", "NW_CITY_TABLE_WAR_01.3DS", "CHESTSMALL_OCCHESTSMALL.MDS", "BARBQ_SCAV.ASC", "OC_GARBAGE_V1.3DS", "OM_OREBOX_01.3DS", "OC_SAECKE_01.3DS", "AW_CRATE_01.3DS", "AW_CRATEPILE_01.3DS", "BEDHIGH_2_OC.ASC", "PC_CRYSTALLIGHT_02.3DS", "OC_BARRELHOLDER_V01.3DS", "OC_BARREL_V1.3DS", "DT_BOOKS_01.3DS", "DT_BOOKSHELF_V2.3DS", "DT_CARPET_ROUND_01.3DS", "OC_DECORATE_V1.3DS", "OC_DECORATE_V4.3DS", "OC_DECORATE_V5.3DS", "DT_FIREPLACE_V1.3DS", "OC_MOB_SHELVES_BIG.3DS", "OC_BIGLIGHTER_01.3DS", "OC_KITCHENSTUFF_V01.3DS", "DT_2ND_FLOOR_BANNKREIS_02.3DS", "OC_MOB_DESK.3DS", "OC_WEAPON_SHELF_EMPTY_V1.3DS", "OC_BARRELS_STRUBBELIG.3DS", "OC_LOB_PLANKBROKEN_SMALL.3DS", "OC_LOB_PLANKBROKEN_SMALL2.3DS", "CHAIR_1_OC.ASC", "OC_DECORATE_V2.3DS", "OC_PICTURE_V2.3DS", "OC_LOB_PLANK_INDOOR_BIG.3DS", "OC_BARRELS_REIHE.3DS", "OC_FIREWOOD_V3.3DS", "BEAM_MS.3DS", "CHESTBIG_OCCHESTMEDIUM.MDS", "OC_DECOWALL_01.3DS", "OC_MOB_SHELVES_SMALL.3DS", "OC_LOB_TABLE_HEAVY.3DS", "OC_FIREWOOD_V2.3DS", "DT_FIREPLACE_V2.3DS", "OC_SACK_V03.3DS", "DT_TABLE_V2.3DS", "DT_LIGHTER_PENTAGRAMM.3DS", "OC_WEAPON_ARMORSHOES.3DS", "OC_LOB_FIREPLACE_HUGE.3DS", "SMOKE_WATERPIPE.MDS", "OC_PICTURE_V1.3DS", "OC_LOB_PLANK_INDOOR.3DS", "OC_DECOWALL_02.3DS", "OC_LOB_STAIRS_02.3DS", "MIN_LOB_HANDRAIL_3.3DS", "THRONE_BIG.ASC", "OC_THRONE_GROUND.3DS", "OC_STABLE_PLANKS.3DS", "BEAM_G.3DS", "BEAM_ML.3DS", "OC_CHICKENSTICK_V01.3DS", "OC_TABLE_V1.3DS", "NC_LOB_NICE.3DS", "OC_FIREPLACE_V2.3DS", "OC_BARREL_V2.3DS", "OC_SHELF_V1.3DS", "OC_TABLE_V2.3DS", "OC_LOB_GATE_SMALL.3DS", "OC_CHICKENSTICK_V02.3DS", "OC_CHICKENSTICK_CHICKEN_V01.3DS", "OC_SACK_V02.3DS", "OC_FIREWOOD_V1.3DS", "BEDHIGH_1_OC.ASC", "OC_LEATHERSTAND_V01.3DS", "OC_LOB_LIGHTER_HUGE3.3DS", "OW_LOB_FENCE_V2.3DS", "REPAIR_PLANK.ASC", "OW_LOB_FENCE_V1.3DS", "CHESTBIG_OCCHESTLARGE.MDS", "CHESTSMALL_OCCHESTSMALLLOCKED.MDS", "OW_ORCTENT_SMALL_01.3DS", "OW_ORCTENT_LARGE_01.3DS", "OW_MISC_TENT_01.3DS", "NW_MISC_FIELDGRAVE_01.3DS", "OW_LOB_WOODPLANKS_V1.3DS", "DT_SKELETON_V01_HEAD.3DS", "MIN_LOB_STONE_V1_30.3DS", "NW_MISC_TREASURE_01.3DS", "NW_NATURE_WATERGRASS_56P.3DS", "NW_NATURE_DUCKWEED_72P.3DS", "NW_NATURE_DUCKWEED_144P.3DS", "NW_NATURE_LIANA_01_87P.3DS", "NW_NATURE_LIANA_01_400P.3DS", "OW_NATURE_BIG_TREE_02_380P.3DS", "NW_NATURE_TANNE_75P.3DS", "NW_NATURE_FARN_102P.3DS", "OW_NATURE_OLDTREE_02.3DS", "NW_NATURE_BAUMSTUMPF_115P.3DS", "NW_NATURE_STONE_CULT_TOP_02.3DS", "MIN_LOB_STONE_V2_30.3DS", "NW_NATURE_GRASSGROUP_01.3DS", "NW_NATURE_BAUMSTUMPF_02_115P.3DS", "NW_NATURE_WATERGRASS_02_100P.3DS", "NW_NATURE_HOHETANNE_59P.3DS", "NW_MISC_BIG_TENT_430P.3DS", "NW_MORPH_ORC_STANDART_01.MMS", "CR_DI_OPENEGG.3DS", "MIN_MOB_STONE_V2_20.3DS", "ORE_GROUND.ASC", "NW_MISC_DUMPTRUCK_01.3DS", "OW_PALISSADE.3DS", "NW_CITY_CART_01.3DS", "NW_NATURE_SMALL_ROOTS_30P.3DS", "NW_NATURE_FENCE_01.3DS", "OW_LOB_DEADTREE_04.3DS", "BENCH_1_OC.ASC", "FIREPLACE_GROUND.ASC", "DT_SKELETON_V01.3DS", "OC_TABLE_V2_DESTROYED.3DS", "PC_LOB_BRIDGE_PLANK_BIG.3DS", "OW_LOB_WOODPLANKS_V5.3DS", "OW_FOCUSPLATTFORM.3DS", "EVT_NC_MAINGATE01.3DS", "ORC_SKULLSONSTAFF.3DS", "ORC_E_COLUMN.3DS", "ORC_BED_01.3DS", "ORC_BROKENCART.3DS", "OW_LOB_TREE_ROOT_V2.3DS", "OW_LOB_TREE_DESTROYED_V2.3DS", "OW_LOB_BUSH_V7.3DS", "OW_LOB_BUSH_V4.3DS", "OC_LOB_BOWTRAIN.3DS", "OW_LOB_DEADTREE_07.3DS", "OW_LOB_TREE_V11.3DS", "OW_LOB_BUSH_REED_V1.3DS", "OW_LOB_BUSH_V1.3DS", "OC_LOB_FLAG_SMALL.3DS", "MIN_LOB_BRIDGESTAND.3DS", "MIN_LOB_STONE_V3_30.3DS", "OW_PLANKHUTII.3DS", "MIN_MOB_STONE_V3_20.3DS", "OW_LOB_FENCE_V3.3DS", "MIN_COB_STONE_V1_10.3DS", "OW_O_LAKEHUT.3DS", "OW_ORETRAIL_PLANK_V01.3DS", "OW_LOB_BUSH_V5.3DS", "OW_LOB_WOODPLANKS_V2.3DS", "OW_LOB_WOODPLANKS_V4.3DS", "OW_LOB_WOODPLANKS_V3.3DS", "OW_LOB_BUSH_V2.3DS", "OW_LOB_BUSH_WATER_V1.3DS", "OW_LOB_TREE_V5.3DS", "OW_LOB_TREE_V6.3DS", "OW_LOB_TREE_V8.3DS", "OW_LOB_TREE_V10.3DS", "OW_LOB_TREE_ROOT_V1.3DS", "MIN_MOB_STONE_V1_20.3DS", "MIN_LOB_STONE_V3_40.3DS", "MIN_COB_STONE_V2_10.3DS", "MIN_COB_STONE_V3_10.3DS", "OW_O_BIGBRIDGE.3DS", "NC_LOB_TABLE1.3DS", "BENCH_1_NC.ASC", "OW_FOREST_HANGHIM_TREE_V3.3DS", "EVT_TPL_GITTERKAEFIG_4X4M.3DS", "OW_FOCUS_V02.3DS", "NW_NATURE_STONE_BIGHIGH_01.3DS", "NW_NATURE_BAUMSTUMPF_154P.3DS", "NW_NATURE_STONE_CULT_PART_02.3DS", "NW_NATURE_BUSH_300P.3DS", "NW_HARBOUR_CRATEBROKEN_01.3DS", "BAUMSAEGE_1.ASC", "OW_MISC_BURNED_WALL_03.3DS", "CHAIR_1_NC.ASC", "NW_MISC_SPIDERWEBS_01.3DS", "NW_NATURE_FARNTEPPICH_612P.3DS", "NW_NATURE_BUSH_25P.3DS", "BEDHIGH_PC.ASC", "NC_LOB_NICE2.3DS", "PAN_OC.MDS", "CHESTBIG_ADD_STONE_OPEN.MDS", "NW_NATURE_BUSH_120P.3DS", "TREASURE_ADDON_01.ASC", "ADDON_STONES_STONE_02_BIG_124P.3DS", "ADDON_STONES_COUPLESTONES_01_230P.3DS", "ADDON_VALLEYPLANT_TREE_02_XL_191P.3DS", "ADDON_VALLEYPLANT_TREE_03_L_344P.3DS", "NW_NATURE_HANGPLANTS_02.3DS", "ADDON_STONES_STONE_03_SMALL_92P.3DS", "NW_NATURE_FARNTEPPICH_306P.3DS", "ADDON_PLANTS_HANGPLANTS_02_494P.3DS", "OW_MUSHROOM_V1.3DS", "ADDON_VALLEYPLANT_TREE_01_XL_357P.3DS", "ADDON_PLANTS_THINTREE_01_200P.3DS", "ADDON_STONES_STONE_04_MED_114P.3DS", "ADDON_STONES_STONE_02_MED_124P.3DS", "MIN_LOB_COUPLESTONES.3DS", "NW_EVT_CEMENTARYCOFFIN_01.3DS", "ADDON_PLANTS_JUNGLETREE_02_163P.3DS", "ADDON_PLANTS_JUNGLETREE_03_163P.3DS", "NW_NATURE_BAUMSTAMM_01_30P.3DS", "ADDON_PLANTS_HANGPLANTS_01_374P.3DS", "NW_NATURE_LONG_BUSH_360P.3DS", "NW_NATURE_SIDEPLANT_BIG_01.3DS", "NW_NATURE_BUSH_60P.3DS", "NW_NATURE_BUSH_56P.3DS", "NW_NATURE_BAUMSTUMPF_03_174P.3DS", "NW_NATURE_SIDEPLANT_CORNER_01.3DS", "ADDON_VALLEYPLANT_TREE_02_L_191P.3DS", "NW_NATURE_STONE_BIGHIGH_02.3DS", "NW_MISC_SKELETON_01.3DS", "ITMI_FOCUS.3DS", "ADDON_MAYA_PILLAR_04.3DS", "CHESTBIG_ADD_STONE_LOCKED.MDS", "INNOS_BELIAR_ADDON_01.ASC", "ADDON_VALLEYPLANT_TREE_01_L_357P.3DS", "ADDON_VALLEYPLANT_TREE_04_XL_304P.3DS", "ADDON_VALLEYPLANT_TREE_03_XL_344P.3DS", "ADDON_CANYONPLANT_TREE_01_1010P.3DS", "NW_MISC_BOARDS_01B.3DS", "ADDON_PLANTS_PALM_LONG_STRAIGHT_01_200P.3DS", "ADDON_PLANTS_PALM_LONG_BENT_01_200P.3DS", "ADDON_PLANTS_PALM_SHORT_BENT_01_170P.3DS", "ADDON_PLANTS_PALM_LONG_BENT_02_200P.3DS", "ADDON_MISC_BOHLEN_01.3DS", "NW_NATURE_BAUMSTAMM_166P.3DS", "NW_CITY_SHIP_01.3DS", "ADDON_CANYONPLANT_BUSHES_01_120P.3DS", "NW_NATURE_FARN_BIG_01.3DS", "NW_NATURE_HANGPLANTS_01.3DS", "ADDON_PLANTS_BUSH_01_36P.3DS", "ADDON_CANYONPLANT_BUSHES_02_240P.3DS", "ADDON_STONES_CRYSTAL_01_228P.3DS", "ADDON_STONES_CRYSTAL_02_228P.3DS", "NW_NATURE_BUSH_01.3DS", "DOOR_NW_POOR_01.MDS", "BEDHIGH_PSI.ASC", "MIN_LOB_BRIDGERAMP.3DS", "MIN_LOB_PLANKS_2X3M.3DS", "NW_CITY_WEAPON_BARREL_01.3DS", "MIN_LOB_BRIDGESTAND_02.3DS", "OW_LOOKOUT.3DS", "ITAT_SHADOWFUR.3DS", "ITAT_WOLFFUR.3DS", "MIN_LOB_COVER_4X7M.3DS", "NW_MISC_BOARDS_01C.3DS", "NW_MISC_BOARDS_02C.3DS", "NW_HARBOUR_SMALL_BOAT_03.3DS", "NW_HARBOUR_FISHINGNET_01.3DS", "ADDON_PLANTS_SMALLPALM_02_73P.3DS", "ADDON_PLANTS_PALM_SHORT_BENT_02_170P.3DS", "ADDON_PLANTS_PALM_SHORT_STRAIGHT_01_170P.3DS", "ADDON_PLANTS_SMALLPALM_01_73P.3DS", "ADDON_STONES_STONE_01_84P.3DS", "MIN_LOB_SOLDIERSHUT.3DS", "NW_CITY_DECO_SWORDFISH_01.3DS", "ADDON_MISC_FLAGMAST_01.3DS", "ADD_PIRATEFLAG.MMS", "ADDON_PLANTS_TREE_01_353P.3DS", "ADDON_DUNGEON_SCROLLS_02.3DS", "OC_SHELF_V3.3DS", "CR_DI_SLIME.3DS",
                "ORE_GROUND_GOLD.ASC", "ADDON_MISC_HOLZBOCK.3DS", "ADDON_MISC_WOODPILLAR_L04.3DS", "ADDON_MISC_WOODPILLAR_M03.3DS", "ADDON_MISC_WOODPILLAR_L01.3DS", "ADDON_MISC_WOODPILLAR_M04.3DS", "ADDON_MISC_WOODPILLAR_L02.3DS", "ADDON_MISC_WOODPILLAR_M01.3DS", "ADDON_GOLDMINE_BRIDGE.3DS", "OM_ORETABLE.3DS", "EVT_CORNERROOMS_LIFT_01.3DS", "NW_NATURE_STONE_CULT_STAND_02.3DS", "ADDON_MISC_HOLZWAND_02.3DS", "ADDON_PLANTS_PALM_BEND_01_199P.3DS", "ADDON_CANYONPLANT_CACTUSBIG_01_165P.3DS", "NW_NATURE_FARN_SMALL_102P.3DS", "ADDON_PLANTS_WALLFERN_02_234PP.3DS", "ADDON_PLANTS_WALLFERN_03_468P.3DS", "ADDON_PLANTS_WALLFERN_01_117P.3DS", "ADDON_PLANTS_TREE_04_275P.3DS", "NW_MISC_BOARDS_02B.3DS", "NW_NATURE_SMALLTREE_79P.3DS", "ADDON_PLANTS_TREE_05_239P.3DS", "BOOK_ADDON_STONEPLATE_01.ASC", "ADDON_DUNGEON_PILLAR_02.3DS", "ADDON_DUNGEON_PILLAR_01.3DS", "EVT_ADDON_LSTTEMP_PLATESSAVE_01.3DS", "ADDON_DUNGEON_SCROLLS_03.3DS", "ADDON_DUNGEON_SCROLLS_01.3DS", "ADDON_DUNGEON_SHELF_01.3DS", "ADDON_DUNGEON_SHELF_03.3DS", "ADDON_DUNGEON_SHELF_02.3DS", "ADDON_DUNGEON_BROKENSTONE_03.3DS", "ADDON_CANYONOBJECT_SIGN_01.3DS", "ADDON_CANYONPLANT_CACTUSBIG_02_165P.3DS", "ADDON_CANYONPLANT_BUSHES_05_168P.3DS", "ADDON_CANYONOBJECT_CAR_01.3DS", "ADDON_STONES_CRYSTAL_ROSE_01_228P.3DS", "ADDON_CANYONPLANT_CACTUSLONG_02_97P.3DS", "ADDON_PLANTS_PALM_STRAIGHT_01_151P.3DS", "ADDON_CANYONPLANT_CACTUSLONG_01_58P.3DS", "ADDON_CANYONPLANT_BUSHES_04_240P.3DS", "NW_MISC_GRAVESTONE_01.3DS", "ADDON_PLANTS_JUNGLETREE_04_163P.3DS", "ADDON_PLANTS_LIANA_BEND_01_120P.3DS", "ADDON_PLANTS_LIANA_LONG_01_27P.3DS", "OW_ORCS_TOTEM_01_200P.3DS", "NW_SEQ_EGG_SIGN_02.TGA", "HERB_PSI.MDS", "ADDON_MAYA_PILLAR_02.3DS", "ADDON_MAYA_SNAKE_01.3DS", "CHESTSMALL_NW_POOR_LOCKED.MDS", "CHESTSMALL_NW_POOR_OPEN.MDS", "NW_MISC_POOR_HUT_01.3DS", "NW_NATURE_WOODPIECE_01.3DS", "ADDON_PLANTS_TREE_03_279P.3DS", "ADDON_MISC_WOODPILLAR_M02.3DS", "NW_CITY_CARPET_02.3DS", "OC_BARREL_V2_DESTROYED.3DS", "NW_NATURE_SIDEPLANT_01.3DS", "CHAIR_3_PC.ASC", "PC_SHOP_V01.3DS", "PC_LOB_TABLE1.3DS", "NC_LOB_TABLE2.3DS", "OC_MOB_TABLE_NORMAL_DEFECT.3DS", "OC_LOB_SHELF_1.3DS", "ORC_E_DEKO_02.3DS", "NW_NATURE_SMALLWEED_24P.3DS", "LADDER_9.ASC", "ADDON_DUNGEON_SCHUTT_01.3DS", "TOUCHPLATE_SEQ_STONE_MEN_01.MDS", "ADDON_LSTTEMP_STONEGUARDIAN_MEDIUM_01.3DS", "TOUCHPLATE_SEQ_STONE_KEY_01.MDS", "ADDON_LSTTEMP_STONEGUARDIAN_LARGE_01.3DS", "EVT_TPL_METALTHORNS_4X4X4M.3DS", "EVT_ADDON_LSTTEMP_SPIKEROOM_04.3DS", "EVT_ADDON_LSTTEMP_SPIKEROOM_03.3DS", "EVT_ADDON_LSTTEMP_SPIKEROOM_02.3DS", "EVT_ADDON_LSTTEMP_SPIKEROOM_01.3DS", "BEAM_T.3DS", "CAMERA.3DS", "CYLINDERFX.3DS", "DEMON_DIE.3DS", "DT_2ND_FLOOR_BANNKREIS.3DS", "DT_2ND_FLOOR_GEWOELBE.3DS", "DT_3RD_FLOOR_GEWOELBE.3DS", "DT_BED_V1.3DS", "DT_BOOKSHELF_V1.3DS", "DT_CHAINBOX.3DS", "DT_CRATE_V1.3DS", "DT_CRATE_V2.3DS", "DT_TABLE_01.3DS", "DT_TORCH_V1.3DS", "EVT_CASTEL_FLOOR_1.3DS", "EVT_CASTEL_FLOOR_2.3DS", "EVT_CASTEL_FLOOR_3.3DS", "EVT_CASTEL_FLOOR_4.3DS", "EVT_CASTEL_FLOOR_5.3DS", "EVT_CASTEL_PLATE.3DS", "EVT_CASTLE_LIFT_01.3DS", "EVT_CASTLE_LIFT_02.3DS", "EVT_CASTLE_LIFT_03.3DS", "EVT_CORNERROOMS_LIFTGATEWALL_01.3DS", "EVT_CORNERROOMS_WHEELWALL_01.3DS", "EVT_DEMONTOWER_LIFT.3DS", "EVT_DEMONTOWER_LIFT_WALL.3DS", "EVT_GATE_LARGE_01.3DS", "EVT_GATE_SMALL_01.3DS", "EVT_GATEROOM_WHEEL_01.3DS", "EVT_GATEROOM_WHEEL_02.3DS", "EVT_MAINHALL_DOOR_LEFT_01.3DS", "EVT_MAINHALL_DOOR_RIGHT_01.3DS", "EVT_MAINHALL_PILLAR_01.3DS", "EVT_MAINHALL_RING_LEFT_01.3DS", "EVT_MAINHALL_RING_RIGHT_01.3DS", "EVT_OC_MAINGATE01_01.3DS", "EVT_OC_MAINGATE01_02.3DS", "EVT_OC_MAINGATE01_03.3DS", "EVT_OC_MAINGATE02_01.3DS", "EVT_OC_MAINGATE02_02.3DS", "EVT_OC_MAINGATE02_03.3DS", "EVT_SPAWN_01.3DS", "EVT_TPL_DECOSTONE_01_DAEMON.3DS", "EVT_TPL_DECOSTONE_01_DOORS_01.3DS", "EVT_TPL_DECOSTONE_01_DOORS_02.3DS", "EVT_TPL_DECOSTONE_01_MUMMY.3DS", "EVT_TPL_DECOSTONE_01_TARGET.3DS", "EVT_TPL_GITTERKAEFIG_4X5M.3DS", "EVT_TPL_METALTHORNS_DEADLY.3DS", "EVT_TPL_STONECEILING_01.3DS", "EVT_WALLMOVER_01.3DS", "FAKESCROLL.3DS", "FMC_BRIDGE_BOTTOM.3DS", "FMC_BRIDGE_TOP.3DS", "GROUNDSHADOW.3DS", "HAMMEL.3DS", "INVISIBLE_CAMERA.3DS", "INVISIBLE_KEYFRAME.3DS", "INVISIBLE_WAYPOINT.3DS", "INVISIBLE_ZCCAMTRJ_KEYFRAME.3DS", "INVISIBLE_ZCCODEMASTER.3DS", "INVISIBLE_ZCEARTHQUAKE.3DS", "INVISIBLE_ZCPFXCONTROLER.3DS", "INVISIBLE_ZCTRIGGER.3DS", "INVISIBLE_ZCTRIGGERCHANGELEVEL.3DS", "INVISIBLE_ZCTRIGGERTELEPORT.3DS", "INVISIBLE_ZCVOBLIGHT.3DS", "INVISIBLE_ZCVOBMARKER.3DS", "INVISIBLE_ZCVOBSOUND.3DS", "INVISIBLE_ZCVOBSOUNDDAYTIME.3DS", "INVISIBLE_ZCVOBSPOT.3DS", "INVISIBLE_ZCVOBSTARTPOINT.3DS", "INVISIBLE_ZCVOBWAYPOINT.3DS", "INVISIBLE_ZCZONEMUSIC.3DS", "INVISIBLE_ZCZONEREVERB.3DS", "INVISIBLE_ZCZONEZFOG.3DS", "MFX_FEAR4.3DS", "MIN_LOB_BRIDGE_30M.3DS", "MIN_LOB_BRIDGE_4X4M.3DS", "MIN_LOB_BRIDGE_ANGEL_4M.3DS", "MIN_LOB_HANDRAIL_1.3DS", "MIN_LOB_HANDRAIL_2.3DS", "MIN_LOB_MELTER.3DS", "MIN_LOB_PLANKS_2X4M.3DS", "MIN_LOB_PLANKS_2X5M.3DS", "MIN_LOB_PLANKS_3X11M.3DS", "MIN_LOB_PLANKS_3X7M.3DS", "MIN_LOB_STANDINGWORKPLANK_5X7M.3DS", "MIN_LOB_STOMPERSTAND.3DS", "MIN_LOB_STONE_V3_50.3DS", "MIN_LOB_WINKELSTEG.3DS", "MIN_LOB_WORKPLANK_4X6M.3DS", "MIN_LOB_WORKPLANK_5X7M.3DS", "MIN_ORE_BIG_V1.3DS", "MIN_ORE_BIG_V2.3DS", "NC_BIRDFRIGHTENER.3DS", "NC_LOB_BAR_TABLE1.3DS", "NC_LOB_HOUSEPLANKS.3DS", "NC_LOB_LIGHTER.3DS", "NC_LOB_LOG1.3DS", "NC_LOB_LOG2.3DS", "NC_LOB_LOG3.3DS", "NC_OREHEAP.3DS", "NC_OREHEAP_FENCE.3DS", "NC_OREHEAP_PFX.3DS", "NC_PLANKS_V01.3DS", "NC_STAIRS_V01.3DS", "NW_ABANDONEDMINE_BETA.3DS", "NW_CITY_BEAM_01.3DS", "NW_CITY_BEAMS_01.3DS", "NW_CITY_BEAMS_02.3DS", "NW_CITY_BEAMS_BOWS_01.3DS", "NW_CITY_BEAMS_FARM_01.3DS", "NW_CITY_BEAMS_FARM_02.3DS", "NW_CITY_BEAMS_MIXSHOP_01.3DS", "NW_CITY_BEAMS_PUB_01.3DS", "NW_CITY_BEAMS_SMITH_01.3DS", "NW_CITY_CARPET_01.3DS", "NW_CITY_COUNTER_01.3DS", "NW_CITY_DECO_PICTURE_02.3DS", "NW_CITY_DECO_SHADOWHEAD_01.3DS", "NW_CITY_DECO_SHIELD_01.3DS", "NW_CITY_DECO_SHIELD_02.3DS", "NW_CITY_DECO_TROLLHEAD_01.3DS", "NW_CITY_FIREPLACE_LARGE_01.3DS", "NW_CITY_FIREPLACE_LARGE_011.3DS", "NW_CITY_FIREPLACE_LARGE_02.3DS", "NW_CITY_FIREPLACE_LARGE_021.3DS", "NW_CITY_FIREPLACE_NORMAL_01.3DS", "NW_CITY_FIREPLACE_NORMAL_011.3DS", "NW_CITY_FLOWERPOT_01.3DS", "NW_CITY_GALLOWS_01.3DS", "NW_CITY_HIDDENDOOR_01.3DS", "NW_CITY_MARKETSTALL_ALCHEMY_01.3DS", "NW_CITY_MARKETSTALL_FOOD_01.3DS", "NW_CITY_MARKETSTALL_JUNKSHOP_01.3DS", "NW_CITY_MARKETSTALL_ROOF_01.3DS", "NW_CITY_MARKETSTALL_WEAPONS_01.3DS", "NW_CITY_SIGN_BOWS_01.3DS", "NW_CITY_SIGN_HOTEL_01.3DS", "NW_CITY_SIGN_MIXSHOP_01.3DS", "NW_CITY_SIGN_PUB_01.3DS", "NW_CITY_SIGN_SMITH_01.3DS", "NW_CITY_STREETLAMP_01.3DS", "NW_CITY_TABLE_PEASANT_02.3DS", "NW_CITY_TABLE_RICH_01.3DS", "NW_CITY_WEAPON_BAG_01.3DS", "NW_CITY_WINDOW_WOOD_IN_01.3DS", "NW_CITY_WINDOW_WOOD_IN_02.3DS", "NW_CITY_WINDOW_WOOD_OUT_01.3DS", "NW_DRAGONISLE_BIGBRIDGE_01.3DS", "NW_DRAGONISLE_BIGDOOR_HEAD_LEFT_01.3DS", "NW_DRAGONISLE_BIGDOOR_HEAD_RIGHT_01.3DS", "NW_DRAGONISLE_BIGDOOR_LEFT_01.3DS", "NW_DRAGONISLE_BIGDOOR_RIGHT_01.3DS", "NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS", "NW_DRAGONISLE_STALAGTITE_01.3DS", "NW_EVT_CEMENTARYSTEP_01.3DS", "NW_EVT_MONASTERYWALL_01.3DS", "NW_HARBOUR_IRONROPEHOLDER_01.3DS", "NW_HARBOUR_IRONROPEHOLDER_WROPE_01.3DS", "NW_HARBOUR_RINGBIG_01.3DS", "NW_HARBOUR_RINGSMALL_01.3DS", "NW_HARBOUR_SMALL_BOAT_01.3DS", "NW_HARBOUR_SMALL_BOAT_02.3DS", "NW_MISC_BOARDS_01A.3DS", "NW_MISC_BOARDS_02A.3DS", "NW_MISC_CAVE_BOARDING_01.3DS", "NW_MISC_CHAIN_01.3DS", "NW_MISC_DECO_INNOS_LARGE_01.3DS", "NW_MISC_HANGROPE.3DS", "NW_MISC_HARPIE_STANDY.3DS", "NW_MISC_MINE_ENTRANCE_01.3DS", "NW_MISC_SIGN_01.3DS", "NW_MISC_STRAW_01.3DS", "NW_MISC_STRAW_02.3DS", "NW_MISC_STRAW_GROUP_01.3DS", "NW_MONASTERY_ALTAR_01.3DS", "NW_NATURE_BAUMSTUMPF_02_154P.3DS", "NW_NATURE_BIGTREE_356P.3DS", "NW_NATURE_BIGTREE_SUNNY_356P.3DS", "NW_NATURE_GREAT_WEED_XGROUP.3DS", "NW_NATURE_HOHETANNE_91P.3DS", "NW_NATURE_HOHETANNE_MIDDLE_59P.3DS", "NW_NATURE_HOHETANNE_VERYHIGH_59P.3DS", "NW_NATURE_HOHETANNEN_01_531P.3DS", "NW_NATURE_PLANT_01.3DS", "NW_NATURE_PLANT_02.3DS", "NW_NATURE_PLANT_03.3DS", "NW_NATURE_SMALLTREE_103P.3DS", "NW_NATURE_STONE_BIGFLAT_01.3DS", "NW_NATURE_STONE_BIGFLAT_02.3DS", "NW_NATURE_STONE_CULT_ALTAR_01.3DS", "NW_NATURE_STONE_CULT_PART_01.3DS", "NW_NATURE_STONE_CULT_PART_03.3DS", "NW_NATURE_STONE_CULT_SCENE_01.3DS", "NW_NATURE_STONE_CULT_TOP_01.3DS", "NW_NATURE_TANNE_100P.3DS", "NW_NATURE_WATERLILI_RED_32P.3DS", "NW_NATURE_WATERLILI_WHITE_32P.3DS", "NW_NATURE_WATERLILI_WHITE_64P.3DS", "NW_ORETRAIL_BETA.3DS", "OC_BARONSLEDGE_V1.3DS", "OC_CHIMNEY_V1.3DS", "OC_CHIMNEY_V2.3DS", "OC_DECORATE_V3.3DS", "OC_DECOROOF_V1.3DS", "OC_DECOROOF_V2.3DS", "OC_DUNGEON_PLANKS.3DS", "OC_EBA_FUR_V1.3DS", "OC_FIREPLACE_V1.3DS", "OC_FIREPLACE_V3.3DS", "OC_FIREPLACEBIG_V01.3DS", "OC_GATE_WELL.3DS", "OC_LOB_CAGE2.3DS", "OC_LOB_GALGEN.3DS", "OC_LOB_GALGEN_V2.3DS", "OC_LOB_GATE_SMALL2.3DS", "OC_LOB_GATE_SMALL3.3DS", "OC_LOB_PLANK_INDOOR_LARGE.3DS", "OC_LOB_STAIRS_03.3DS", "OC_LOB_STAIRS_05.3DS", "OC_LOB_STAIRS_10.3DS", "OC_LOB_STAIRS_ROUND.3DS", "OC_LOB_STONE.3DS", "OC_LOB_STONEBROKEN.3DS", "OC_LOB_TABLE_EBA.3DS", "OC_LOB_TABLE_HEAVY_LONG.3DS", "OC_LOB_WASTER.3DS", "OC_MOB_BRAKER.3DS,", "OC_MOB_SHELVES_MEDIUM.3DS", "OC_OLDSAWHOUSE_V1.3DS", "OC_SACK_V01.3DS", "OC_SAECKE_02.3DS", "OC_SHELF_V2.3DS", "OC_STAIRS_V01.3DS", "OC_TABLE_V1_DESTROYED.3DS", "OC_WEAPON_ARMORHOLDER.3DS", "OC_WEAPON_SHELF_EMPTY_V2.3DS", "OC_WEAPON_SHELF_NEW_V1.3DS", "OC_WEAPON_SHELF_NEW_V2.3DS", "OC_WEAPON_SHELF_OLD_V1.3DS", "OC_WEAPON_SHELF_OLD_V2.3DS", "OC_WELL_V1.3DS", "OM_OREHEAP_SMALL_01.3DS", "ORC_DECO_V1.3DS", "ORC_E_BED.3DS", "ORC_E_BRIDGE_3X4M.3DS", "ORC_E_WALL.3DS", "ORC_E_WALL_02.3DS", "ORC_FIREPLACE_V1.3DS", "ORC_FIREPLACE_V2.3DS", "ORC_FIREPLACE_V4.3DS", "ORC_FIREPLACE_V5.3DS", "ORC_FIREPLACE_V6.3DS", "ORC_GATE_5X5M.3DS", "ORC_MASTERTHRONE.3DS", "ORC_MOVER_01_3X5XM.3DS", "ORC_MOVER_01_5X9M.3DS", "ORC_PLANKS_2X3M.3DS", "ORC_SQUAREPLANKS_2X3M.3DS", "ORC_STANDING_LAMP.3DS", "ORC_STATUE_01.3DS", "ORC_STATUE_02.3DS", "ORC_STATUE_03.3DS",
                "ORC_TOWER_V01.3DS", "ORC_VASE_V1.3DS", "ORC_WALL_FIRE_01.3DS", "ORC_WALL_FIRE_02.3DS", "ORCDUMMYZELT.3DS", "ORCDUMMYZELT_LARGE.3DS", "OW_BROKENHUT.3DS", "OW_BUSHES_01.3DS", "OW_FOCUS_V01.3DS", "OW_FOREST_TREE_V2.3DS", "OW_FOREST_TREE_V3.3DS", "OW_FOREST_TREE_V4.3DS", "OW_FOREST_TREE_V5.3DS", "OW_LOB_BUSH_V8.3DS", "OW_LOB_DEADTREE_06.3DS", "OW_LOB_PSIBUSHES_V1.3DS", "OW_LOB_RICEPLANT_COUPLE.3DS", "OW_LOB_RICEPLANT_ONE.3DS", "OW_LOB_TREE_V12.3DS", "OW_LOB_TREE_V13.3DS", "OW_LOB_TREE_V2.3DS", "OW_LOB_TREE_V3.3DS", "OW_LOB_TREE_V4.3DS", "OW_LOB_TREE_V9.3DS", "OW_LOB_TREEGROUP_V1.3DS", "OW_LOB_TREEGROUP_V2.3DS", "OW_LOB_TREEGROUP_V3.3DS", "OW_LONEHUT.3DS", "OW_MAGICSTAFF_01.3DS", "OW_MISC_BURNED_PLANK_01.3DS", "OW_MISC_BURNED_POLE_01.3DS", "OW_MISC_BURNED_WALL_01.3DS", "OW_MISC_BURNED_WALL_02.3DS", "OW_NATURE_DEADTREE_01.3DS", "OW_O_BIGBRIDGEMIDDLE.3DS", "OW_ORC_STANDART_01.3DS", "OW_ORCS_TOTEM_02_1000P.3DS", "OW_ORETRAIL_PLANK_V02.3DS", "OW_SHIPWRECK_TAIL.3DS", "OW_TROLLPALISSADE.3DS", "OW_TUNNELCOVER.3DS", "PC_BRIDGE_1.3DS", "PC_BRIDGE_2.3DS", "PC_BRIDGE_3.3DS", "PC_BRIDGE_4.3DS", "PC_BRIDGE_ROOF_1.3DS", "PC_FIREPLACE.3DS", "PC_LOB_BRIDGE.3DS", "PC_LOB_BRIDGE_PLANK_HUGE.3DS", "PC_LOB_BRIDGE_PLANK_MINI.3DS", "PC_LOB_BRIDGE_PLANK_SMALL.3DS", "PC_LOB_BRIDGE2.3DS", "PC_LOB_BRIDGE3.3DS", "PC_LOB_FIREBOWL.3DS", "PC_LOB_LOG1.3DS", "PC_LOB_LOG2.3DS", "PC_LOB_SLEEPER2.3DS", "PC_LOB_SLEEPER3.3DS", "PC_LOB_SLEEPER4.3DS", "PC_LOB_TABLE2.3DS", "SPHERE.3DS", "STALAG_V01.3DS", "STALAG_V02.3DS", "STALAG_V03.3DS", "TELEPORT_AURA.3DS", "TPL_BRIDGE_V1.3DS", "TPL_BRIDGE_V2.3DS", "TPL_DECOHEAD_V1.3DS", "TPL_DOORDECO_V1.3DS", "TPL_EVT_BRIDGEBROKEN_01.3DS", "TPL_EVT_BRIDGEFUNCTIONAL_01.3DS", "TPL_EVT_BRIDGESTONE_01.3DS", "TPL_EVT_BRIDGESTONE_02.3DS", "TPL_EVT_BRIDGEWHEEL_01.3DS", "TPL_EVT_ELEVATOR_01.3DS", "TPL_EVT_ELEVATORSTONE_1X1M.3DS", "TPL_EVT_HINT_01.3DS", "TPL_EVT_HINT_02.3DS", "TPL_EVT_JUMPPLATE_01.3DS", "TPL_EVT_NORMALSTONE_1X1M.3DS", "TPL_EVT_PLATEFIRE_01.3DS", "TPL_EVT_PLATEMOON_01.3DS", "TPL_EVT_PLATESUN_01.3DS", "TPL_EVT_PLATEWATER_01.3DS", "TPL_EVT_PURPURSKULLSTONE_1X1M.3DS", "TPL_EVT_SECRETSTONE_01.3DS", "TPL_EVT_SECRETWALL_01.3DS", "TPL_EVT_SECRETWALL_02.3DS", "TPL_EVT_TARGETSTONE_01.3DS", "TPL_LIGHTER_V1.3DS", "TPL_LIGHTER_V2.3DS", "TPL_ORCSTATUE.3DS", "ADDON_DUNGEON_BROKENSTONE_02.3DS", "ADDON_MAYA_PILLAR_03.3DS", "ADDON_MISC_HOLZSTUETZEN_01.3DS", "ADDON_MISC_HOLZWAND_01.3DS", "ADDON_PLANTS_BLAETTERDACH_01_94P.3DS", "ADDON_PLANTS_BLAETTERDACH_02_6P.3DS", "ADDON_PLANTS_DEADTREE_01_1713P.3DS", "ADDON_PLANTS_JUNGLETREE_01_1845P.3DS", "ADDON_PLANTS_TREE_02_1713P.3DS", "ADDON_STONES_CRYSTAL_ROSE_02_228P.3DS", "EVT_ADDON_LSTTEMP_DOOR_BIGHEAD_01.3DS", "EVT_ADDON_LSTTEMP_DOOR_LEFT_01.3DS", "EVT_ADDON_LSTTEMP_DOOR_RIGHT_01.3DS", "EVT_ADDON_LSTTEMP_DOORPLATE_01.3DS", "EVT_ADDON_LSTTEMP_DOORPLATE_02.3DS", "EVT_ADDON_LSTTEMP_FALLSPIKES_01.3DS", "EVT_ADDON_LSTTEMP_PLATESDEAD_01.3DS", "EVT_ADDON_MAYA_PARTICEL_GATEDUMMY.3DS", "EVT_ADDON_MAYA_PARTICEL_VORTEXDUMMY.3DS", "EVT_ADDON_MAYA_STONEMOVER_01.3DS", "EVT_ADDON_NW_TEMPELDOOR_01.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_01.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_02.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_03.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_04.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_05.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_06.3DS", "EVT_ADDON_NW_TROLLPORTAL_PORTAL_07.3DS", "ICELANCE.3DS", "ONION.3DS", "SKULL.3DS", "NW_HARBOUR_ROPE_01.3DS"];
            var items = [];
            for (const element of vob) {
                items.push(new vscode.CompletionItem(element));
            }
            return items;
        }
    }
    return null;
}

function provideCompletiontextSetFont(line, document, position) {
    let match;
    let reg = new RegExp('(textSetFont)' + data.provideFuncCompletionItemsparamsRegex.source, 'g');
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let animations = ["FONT_DEFAULT.TGA", "FONT_10_BOOK.TGA", "FONT_10_BOOK_HI.TGA", "FONT_20_BOOK.TGA", "FONT_20_BOOK_HI.TGA", "FONT_OLD_10_WHITE.TGA", "FONT_OLD_10_WHITE_HI.TGA", "FONT_OLD_20_WHITE.TGA", "FONT_OLD_20_WHITE_HI.TGA"];
            var items = [];
            for (const element of animations) {
                items.push(new vscode.CompletionItem(element));
            }
            return items;
        }
    }
    return null;
}

function provideCompletionFont(line, document, position) {
    let match;
    let reg = /(.font)\s*=\s*"{1}/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            let animations = ["FONT_DEFAULT.TGA", "FONT_10_BOOK.TGA", "FONT_10_BOOK_HI.TGA", "FONT_20_BOOK.TGA", "FONT_20_BOOK_HI.TGA", "FONT_OLD_10_WHITE.TGA", "FONT_OLD_10_WHITE_HI.TGA", "FONT_OLD_20_WHITE.TGA", "FONT_OLD_20_WHITE_HI.TGA"];
            var items = [];
            for (const element of animations) {
                items.push(new vscode.CompletionItem(element));
            }
            return items;
        }
    }
    return null;
}

function provideCompletionPlayerInstance(line, document, position) {
    let match;
    let reg = /(setPlayerInstance)\s*\(.*,\s"/g;
    while (match = reg.exec(line.text.slice(0, position.character))) {
        if (match.index + match[0].length === position.character) {
            var items = [
                CompletionItem('FIREWARAN', "Fire Lizard"),
                CompletionItem('FOLLOW_SHEEP', "Betsy"),
                CompletionItem('FOLLOW_SHEEP_AKIL', "Sheep"),
                CompletionItem('GAANS_SNAPPER', "Snorting Dragon Snapper"),
                CompletionItem('GIANT_BUG', "Field Raider"),
                CompletionItem('GIANT_RAT', "Giant Rat"),
                CompletionItem('GOBBO_BLACK', "Black Goblin"),
                CompletionItem('GOBBO_GREEN', "Goblin"),
                CompletionItem('GRIMBALD_SNAPPER1', "Snapper"),
                CompletionItem('HAMMEL', "Sheep"),
                CompletionItem('HARPIE', "Harpy"),
                CompletionItem('KERVO_LURKER1', "Lurker"),
                CompletionItem('LOBARTS_GIANT_BUG1', "Field Raider"),
                CompletionItem('LURKER', "Lurker"),
                CompletionItem('MAYA_TROLL', "Troll"),
                CompletionItem('MEATBUG', "Meatbug"),
                CompletionItem('MEATBUG_BRUTUS1', "Meatbug"),
                CompletionItem('MINECRAWLER', "Minecrawler"),
                CompletionItem('MINECRAWLERWARRIOR', "Minecrawler Warrior"),
                CompletionItem('MOLERAT', "Molerat"),
                CompletionItem('NEWMINE_LEADSNAPPER', "Pack Leader"),
                CompletionItem('NEWMINE_SNAPPER1', "Snapper"),
                CompletionItem('PEPES_YWOLF1', "Young Wolf"),
                CompletionItem('SCAVENGER', "Scavenger"),
                CompletionItem('SHADOWBEAST', "Shadowbeast"),
                CompletionItem('SHEEP', "Sheep"),
                CompletionItem('SNAPPER', "Snapper"),
                CompletionItem('SUMMONED_WOLF', "Summoned Wolf"),
                CompletionItem('SWAMPSHARK', "Swampshark"),
                CompletionItem('TROLL', "Troll"),
                CompletionItem('TROLL_BLACK', "Black Troll"),
                CompletionItem('TROLL_DI', "Cave Troll"),
                CompletionItem('WARAN', "Lizard"),
                CompletionItem('WARG', "Warg"),
                CompletionItem('WOLF', "Wolf"),
                CompletionItem('WISP', "Ghost Light"),
                CompletionItem('YBLOODFLY', "Small Bloodfly"),
                CompletionItem('YGIANT_BUG', "Young Field Raider"),
                CompletionItem('YGIANT_RAT', "Young Giant Rat"),
                CompletionItem('YGOBBO_GREEN', "Goblin"),
                CompletionItem('YWOLF', "Young Wolf"),
                CompletionItem('CRYPT_SKELETON_LORD', "Shadow Lord Inubis"),
                CompletionItem('DEMON', "Demon"),
                CompletionItem('DEMONLORD', "Demon Lord"),
                CompletionItem('DMT_1299_OBERDEMENTOR_DI', "Black Magician"),
                CompletionItem('DMT_DEMENTORAMBIENT', "Seeker"),
                CompletionItem('DMT_DEMENTORAMBIENTSEKOB1', "Seeker"),
                CompletionItem('DMT_DEMENTORAMBIENTSPEAKER', "Seeker"),
                CompletionItem('DMT_DEMENTORAMBIENTWALKER1', "Seeker"),
                CompletionItem('DMT_DEMENTORAMBIENTWALKER_DI_01', "Seeker"),
                CompletionItem('DMT_DEMENTORSPEAKERVINO1', "Seeker"),
                CompletionItem('DRACONIAN', "Lizard Man"),
                CompletionItem('DRAGON_FIRE', "Feomathar"),
                CompletionItem('DRAGON_FIRE_ISLAND', "Feodaran"),
                CompletionItem('DRAGON_ICE', "Finkregh"),
                CompletionItem('DRAGON_ROCK', "Pedrakhan"),
                CompletionItem('DRAGON_SWAMP', "Pandrodor"),
                CompletionItem('DRAGON_UNDEAD', "Undead Dragon"),
                CompletionItem('DRAGONISLE_KEYMASTER', "Key Master"),
                CompletionItem('FIREGOLEM', "Fire Golem"),
                CompletionItem('GOBBO_SKELETON', "Goblin Skeleton"),
                CompletionItem('GOBBO_SKELETONOWDEMONTOWER', "Goblin Skeleton"),
                CompletionItem('ICEGOLEM', "Ice Golem"),
                CompletionItem('ICEGOLEM_SYLVIO1', "Ice Golem"),
                CompletionItem('LESSER_SKELETON', "Lesser Skeleton"),
                CompletionItem('MAGICGOLEM', "Magic Golem"),
                CompletionItem('SECRETLIBRARYSKELETON', "Skeleton Mage"),
                CompletionItem('SHADOWBEAST_SKELETON', "Shadowbeast Skeleton"),
                CompletionItem('SHADOWBEAST_SKELETON_ANGAR', "Shadowbeast Skeleton"),
                CompletionItem('SHATTERED_GOLEM', "Steingolem"),
                CompletionItem('SKELETON SKELETT', "Skeleton"),
                CompletionItem('SKELETON_ARCHOL1', "Skeleton"),
                CompletionItem('SKELETON_LORD Shadow', "Lord"),
                CompletionItem('SKELETON_LORD_ARCHOL', "Shadow Lord Archol"),
                CompletionItem('SKELETON_MARIO1', "Skeleton"),
                CompletionItem('SKELETONMAGE', "Skeleton Mage"),
                CompletionItem('SKELETONMAGE_ANGAR', "Skeleton Mage"),
                CompletionItem('STONEGOLEM', "Stone Golem"),
                CompletionItem('SUMMONED_DEMON', "Summoned Demon"),
                CompletionItem('SUMMONED_GOBBO_SKELETON', "Summoned Skeleton"),
                CompletionItem('SUMMONED_GOLEM', "Summoned Golem"),
                CompletionItem('SUMMONED_SKELETON', "Summoned Skeleton"),
                CompletionItem('XARDAS_DT_DEMON1', "Demon"),
                CompletionItem('ZOMBIE01', "Zombie"),
                CompletionItem('NONE_110_URSHAK', "Ur-Shak"),
                CompletionItem('ORCELITE_ANTIPALADIN', "Orcish Warlord"),
                CompletionItem('ORCELITE_ANTIPALADIN1', "Orcish Warlord"),
                CompletionItem('ORCELITE_DIOBERST1_REST', "Orc Elite"),
                CompletionItem('ORCELITE_REST', "Orc Elite"),
                CompletionItem('ORCELITE_ROAM', "Orc Elite"),
                CompletionItem('ORCSHAMAN_HOSH_PAK', "Hosh-Pak"),
                CompletionItem('ORCSHAMAN_SIT', "Orc Shaman"),
                CompletionItem('ORCWARRIOR_HARAD', "Orc Scout"),
                CompletionItem('ORCWARRIOR_LOBART1', "Orc Warrior"),
                CompletionItem('ORCWARRIOR_REST', "Orc Warrior"),
                CompletionItem('ORCWARRIOR_ROAM', "Orc Warrior"),
                CompletionItem('ORKELITE_ANTIPALADINORKOBERST', "Orc Colonel"),
                CompletionItem('ORKELITE_ANTIPALADINORKOBERST_DI', "Orc Colonel"),
                CompletionItem('UNDEADORCWARRIOR', "Undead Orc"),
                CompletionItem('DRAGONSNAPPER', "Dragon Snapper"),
                CompletionItem('BALTHASAR_SHEEP1', "Sheep"),
                CompletionItem('BLACKWOLF Blackwold', "BLOODFLY Bloodfly"),
            ];
            return items;
        }
    }
    return null;

}

// function provideCompletionSetPlayerVisual(line, document, position) {
// class provideCompletionSetPlayerVisual extends vscode.CompletionItem {
//     provideCompletionItems(document, position, token, context) {
//         const line = document.lineAt(position.line);
//         let Idx = line.text.lastIndexOf('(', position.character)
//         if (Idx === -1) return ([])

//         return new Promise((resolve, reject) => {
//             let match;
//             let reg = /(setPlayerVisual)\s*\((?>[0-9a-zA-Z_$]*|(?>"[!#-~]*"))*\s*,\s*"/g;
//             let reg2 = /(setPlayerVisual)\s*\((?>[0-9a-zA-Z_$]*|(?>"[!#-~]*"))*\s*,\s*"/g;
//             let extractArgsRegex = /([^,]+\(.+?\))|([^,]+)/g;
//             while (match = reg.exec(line.text.slice(0, position.character))) {
//                 if (match.index + match[0].length === position.character) {
//                     let head = ["Hum_Head_Pony", "Hum_Head_Fighter", "Hum_Head_FatBald", "Hum_Head_Bald", "Hum_Head_Thief", "Hum_Head_Psionic", "Hum_Head_Babe", "Hum_Head_BabeHair", "Hum_Head_Babe1", "Hum_Head_Babe2", "Hum_Head_Babe3", "Hum_Head_Babe4", "Hum_Head_Babe5", "Hum_Head_Babe6", "Hum_Head_Babe7", "Hum_Head_Babe8"];
//                     var items = [
//                         CompletionItem('Hum_Body_Naked0', "Male"),
//                         CompletionItem('Hum_Body_Babe0', "Female"),
//                     ];
//                     resolve(items);
//                 }
//             }
//             resolve([]);
//         });
//     }
// }

function GetConstants(Constants, category) {
    for (const element of Constants) {
        if (element.category == category) {
            return element.completion;
        }
    }
    return [];
}

function CompletionItem(text, doc = null) {
    let item = new vscode.CompletionItem(text);
    item.documentation = doc;
    return item;
}

function getPosition(string, subString, index) {
    return string.split(subString, index).join(subString).length;
}

module.exports = { provideFuncCompletionItems, provideFuncCompletionItems2 }