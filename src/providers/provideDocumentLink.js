const vscode = require('vscode');
const statusbar = require('../statusbar');
const data = require('../data');


class provideDocumentLinks {
    /**
     * @param {vscode.TextDocument} document
     * @param {vscode.CancellationToken} token
     * @returns {[vscode.DocumentLink]}
     */
    provideDocumentLinks(document, token) {
        return new Promise((resolve, reject) => {
            let match;
            const doc = data.docs.DOCS2;
            let txt = document.getText();
            let temp = []
            while (match = data.provideDocumentLinkreg.exec(txt)) {
                if (match) {
                    for (const element of doc.functions) {
                        if (match[1] == element.name && (statusbar.serverclient.text == element.side || element.side == "shared")) {
                            temp.push(new vscode.DocumentLink(document.getWordRangeAtPosition(document.positionAt(match.index)), GetURL(element, "functions", doc.version)))
                        }
                    }
                    for (const element of doc.globals) {
                        if (match[1] == element.name && (statusbar.serverclient.text == element.side || element.side == "shared")) {
                            temp.push(new vscode.DocumentLink(document.getWordRangeAtPosition(document.positionAt(match.index)), GetURL(element, "globals", doc.version)))
                        }
                    }
                    for (const element of GetConstants(doc.constants)) {
                        if (match[1] == element.name && (statusbar.serverclient.text == element.side || element.side == "shared")) {
                            temp.push(new vscode.DocumentLink(document.getWordRangeAtPosition(document.positionAt(match.index)), GetURL(element, "constants", doc.version)))
                        }
                    }
                    for (const element of doc.events) {
                        if (match[1] == element.name && (statusbar.serverclient.text == element.side || element.side == "shared")) {
                            temp.push(new vscode.DocumentLink(document.getWordRangeAtPosition(document.positionAt(match.index)), GetURL(element, "events", doc.version)))
                        }
                    }
                    for (const element of doc.classes) {
                        if (match[1] == element.definition.name && (statusbar.serverclient.text == element.definition.side || element.definition.side == "shared")) {
                            temp.push(new vscode.DocumentLink(document.getWordRangeAtPosition(document.positionAt(match.index)), GetURL(element.definition, "classes", doc.version)))
                        }
                    }
                }
            }
            resolve(temp);
        })
    }
}

function GetURL(element, type, version) {
    return vscode.Uri.parse("https://gothicmultiplayerteam.gitlab.io/docs/" + version + "/script-reference/" + GetSide(element, type) + GetCategory(element, type) + GetName(element, type))
}

function GetCategory(element, type) {
    if (element.category)
        return element.category.toLowerCase().replace(" ", "-") + "/"
    else {
        if (type == "events")
            return "general" + "/"
        else
            return ""
    }
}

function GetName(element, type) {
    if (type == "functions")
        return element.name + "/"
    else if (type == "events")
        return element.name + "/"
    else if (type == "classes")
        return element.name + "/"
    else if (type == "globals")
        return element.name.toLowerCase() + "/"
    else if (type == "constants")
        return ""
}

function GetSide(element, type) {
    return element.side + "-" + type + "/"
}

function GetConstants(constants) {
    let temp = [];
    for (const element of constants) {
        for (const element2 of element.elements) {
            temp.push({
                category: element.category,
                name: element2.name,
                side: element2.side
            })
        }
    }
    return temp
}


module.exports = { provideDocumentLinks }