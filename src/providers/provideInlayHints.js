const vscode = require('vscode');
const utility = require('../utility');
const statusbar = require('../statusbar');
const configuration = vscode.workspace.getConfiguration('gothic-online', undefined)
const data = require('../data');


class provideInlayHints extends vscode.InlayHint {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @returns {vscode.T[]}
    */
    provideInlayHints(document, range, token) {
        return new Promise((resolve, reject) => {
            let match4;
            let match5;
            let temp = [];
            let txt = document.getText();
            while (match4 = data.provideInlayHintsreg2.exec(txt)) {
                var koniec = txt.length;
                var j = 1;
                for (var i = match4.index + match4[0].length; i < txt.length; i++) {
                    if (txt[i] == ")") j = j - 1;
                    else if (txt[i] == "(") j = j + 1;
                    if (j == 0) {
                        koniec = i;
                        break;
                    }
                }
                koniec++;
                let start = document.positionAt(match4.index + match4[0].length);
                let end = document.positionAt(koniec - 1);
                let ran = new vscode.Range(new vscode.Position(start.line, start.character), new vscode.Position(end.line, end.character));
                var temp2 = {
                    name: match4[1].replace(/\s*\(/, ""),
                    parameters: [],
                    position: [],
                }
                while (match5 = data.provideInlayHintsregforparameters.exec(document.getText(ran))) {
                    temp2.parameters.push(match5[0]);
                    temp2.position.push(document.positionAt(match4.index + match4[0].length + match5.index));
                }
                temp.push(temp2);
            }
            let tab = [];
            if (statusbar.serverclient.text == "client") {
                tab = GetInlayHints(tab, data.docs.ClientSignature, temp);
            }
            else if (statusbar.serverclient.text == "server") {
                tab = GetInlayHints(tab, data.docs.ServerSignature, temp);
            }
            else {
                tab = GetInlayHints(tab, data.docs.SharedSignature, temp);
            }
            resolve(tab);
        });
    }
}

function CheckExists(tab, check) {
    for (const element of tab) {
        if (utility.deepEqual(element, check)) {
            return true;
        }
    }
    return false;
}

function GetInlayHints(tab, doc, temp2) {
    for (const elementtemp of temp2) {
        for (const element of doc) {
            if (element.name == elementtemp.name) {
                for (let i = 0; i < element.parameters.length; i++) {
                    if (configuration.get('InlayHints') == "both")
                        var temp = new vscode.InlayHint(elementtemp.position[i], element.parameters[i].label + ":" + element.parameters[i].type, vscode.InlayHintKind.Parameter)
                    else if (configuration.get('InlayHints') == "params")
                        var temp = new vscode.InlayHint(elementtemp.position[i], element.parameters[i].label + ":", vscode.InlayHintKind.Parameter)
                    else if (configuration.get('InlayHints') == "type")
                        var temp = new vscode.InlayHint(elementtemp.position[i], element.parameters[i].type + ":", vscode.InlayHintKind.Type)
                    temp.tooltip = new vscode.MarkdownString(element.parameters[i].documentation);
                    temp.paddingRight = true;
                    temp.name = element.name;
                    if (!CheckExists(tab, temp)) {
                        tab.push(temp);
                    }
                }
            }
        }
    }
    //To-DO console.log(tab)            //buggy in "_title = Draw(anx(5), any(5), "Network debug"),"
    return tab;
}

module.exports = { provideInlayHints }