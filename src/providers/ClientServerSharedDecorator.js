const vscode = require('vscode');
const reg = /([\w.]+)(?=\s?\()/g;
const path = require('path');
const files = require('../Files');
const data = require('../data');

let timeout = undefined;

/**
* @param {vscode.TextEditor} activeEditor
*/
function updateDecorations(activeEditor) {
    if (timeout) {
        clearTimeout(timeout);
        timeout = undefined;
    }
    timeout = setTimeout(function () {
        run(activeEditor);
    }, 200)

}

/**
* @param {vscode.TextEditor} activeEditor
*/
function run(activeEditor) {
    if (!activeEditor) {
        return;
    }
    if (path.extname(activeEditor.document.fileName) != ".nut") {
        return;
    }
   
    const doc = data.docs.DOCS2;
    let text = activeEditor.document.getText();
    let client = [];
    let server = [];
    let shared = [];
    let match;
    if (doc.functions) {
        while (match = reg.exec(text)) {
            for (const element of doc.functions) {
                if (element.name == match[1]) {
                    let startPos = activeEditor.document.positionAt(match.index);
                    let endPos = activeEditor.document.positionAt(match.index + match[1].length);
                    let range = new vscode.Range(startPos, endPos);
                    if (element.side == "client") {
                        client.push(range);
                    }
                    else if (element.side == "server") {
                        server.push(range);
                    }
                    else if (element.side == "shared") {
                        shared.push(range);
                    }
                }
            }
        }
    }
    activeEditor.setDecorations(data.DecorationsClient, []);
    activeEditor.setDecorations(data.DecorationsServer, []);
    activeEditor.setDecorations(data.DecorationsShared, []);
    let temp = files.FindElement(activeEditor.document.fileName) // to fix
    if (temp.type == "server") {
        activeEditor.setDecorations(data.DecorationsServer, server);
        activeEditor.setDecorations(data.DecorationsShared, shared);
    } else if (temp.type == "client") {
        activeEditor.setDecorations(data.DecorationsClient, client);
        activeEditor.setDecorations(data.DecorationsShared, shared);
    }
    else {
        activeEditor.setDecorations(data.DecorationsClient, client);
        activeEditor.setDecorations(data.DecorationsServer, server);
        activeEditor.setDecorations(data.DecorationsShared, shared);
    }
}

module.exports = { updateDecorations }