const vscode = require('vscode');
const item = require('../Items/Item');
const statusbar = require('../statusbar')
const data = require('../data');

class provideCompletionItems extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        return new Promise((resolve, reject) => {
            if (statusbar.serverclient.text == "server") {
                resolve(data.docs.ServerCompletion);
            }
            else if (statusbar.serverclient.text == "client") {
                resolve(data.docs.ClientCompletion);
            }
            else {
                resolve(data.docs.SharedCompletion);
            }
        });
    }
}

class provideCompletionItemsDOT extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, contex) {
        let line = document.lineAt(position.line)
        let dotIdx = line.text.lastIndexOf('.', position.character)
        if (dotIdx === -1) {
            return [];
        }
        const commentIndex = line.text.indexOf('//');
        if (commentIndex >= 0 && position.character > commentIndex) {
            return [];
        }
        return new Promise((resolve, reject) => {
            const range = document.getWordRangeAtPosition(position.with(position.line, position.character - 1));
            const word = document.getText(range);
            var variable = new item.variable(document);
            for (const docs of data.docs.aDOT) {
                if (docs.static == true) {
                    if (word == docs.name) {
                        if (docs.side == statusbar.serverclient.text || docs.side == "shared" || statusbar.serverclient.text == undefined) {
                            resolve(docs.Completion);
                        }
                    }
                } else {
                    for (const variableList of variable.list) {
                        if (word == variableList.name) {
                            if (docs.name == variableList.value) {
                                if (docs.side == statusbar.serverclient.text || docs.side == "shared" || statusbar.serverclient.text == undefined) {
                                    resolve(docs.Completion);
                                }
                            }
                        }
                    }
                }
            }
            resolve([]);
        });
    }
}

module.exports = { provideCompletionItems, provideCompletionItemsDOT }