const vscode = require('vscode');

//onPlayerMessage, onPlayerChangeColor, onPlayerChangeColor, Draw (setColor), Draw3d (setColor, setLineColor), Line (setColor), Line2d (setColor), Line3d (setColor), Texture (setColor)
class ColorPresentation extends vscode.Color {
    /**
    * @param {vscode.Color} color
    * @param {{ document: TextDocument, range: Range }} context
    * @param {vscode.CancellationToken} token
    * @returns {vscode.ColorPresentation[]}
    */
    provideColorPresentations(color, context, token) {
        return new Promise((resolve, reject) => {
            var reg = /(?:(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})(?:,\s*(\d{1,3}))*)|\(\s*(?:0x)?\s*(-*\d*)\)/g;
            let line = context.document.getText(context.range);
            let txt = color.red * 255 + ", " + color.green * 255 + ", " + color.blue * 255
            let match;
            while (match = reg.exec(line)) {
                if (match[4] != undefined) {
                    txt += ", " + Math.round(color.alpha * 255);
                }
                if (match[5] != undefined) {
                    txt = "(" + RGBAtoInt(color.red * 255, color.green * 255, color.blue * 255, color.alpha * 255) + ")"
                }
            }
            let newtext = line.replace(reg, txt);
            let item = new vscode.ColorPresentation(" ");
            item.textEdit = new vscode.TextEdit(context.range, newtext);
            resolve([item]);
        });
    }
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.CancellationToken} token
    * @returns {Array}
    */
    provideDocumentColors(document, token) {
        return new Promise((resolve, reject) => {
            var reg = [/sendMessageToAll\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /sendMessageToPlayer\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /sendPlayerMessageToAll\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /sendPlayerMessageToPlayer\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /setColor\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,  // deprected in 0.3.0
                /rgbToHex\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /setPlayerColor\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g,
                /Sky\.setFogColor\((\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g, // deprected in 0.3.0
                /Sky\.setCloudsColor\((\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g, // deprected in 0.3.0
                /Sky\.setLightingColor\((\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g, // deprected in 0.3.0
                /Sky\.setPlanetColor\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3}),\s*(\d{1,3})/g, // deprected in 0.3.0
                /(?:drawLine|drawLine3d)\(.*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})/g, //buggy in provideColorPresentations
                /(?<=\s)Color\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3}),*\s*(\d{1,3})*/g,
                /(?<=\s)Color\((?:0x)?\s*(-*\d*)\)/g
            ];
            //drawLine(0, 0, 0, 0, 255, 255, 255)
            //drawLine3d(0, 0, 0, 0, 0, 0, 255, 255, 255)
            const text = document.getText();
            const largeNumbers = [];
            let match;
            for (const element of reg) {
                while (match = element.exec(text)) {
                    const startPos = document.positionAt(match.index);
                    const endPos = document.positionAt(match.index + match[0].length);
                    const range = new vscode.Range(startPos, endPos);
                    if (match[2] == undefined && match[3] == undefined && match[4] == undefined) {
                        if (match[1].indexOf("0x") == -1) {
                            let rgb = IntToColor(match[1])
                            if (rgb == null)
                                continue;
                            largeNumbers.push(new vscode.ColorInformation(range, new vscode.Color(rgb.r / 255, rgb.g / 255, rgb.b / 255, rgb.a / 255 || 255)));
                        }
                    } else {
                        largeNumbers.push(new vscode.ColorInformation(range, new vscode.Color(match[1] / 255, match[2] / 255, match[3] / 255, match[4] / 255 || 255)));
                    }
                }
            }
            resolve(largeNumbers);
        });
    }
}

function RGBAtoInt(r, g, b, a) {
    return (b << 24) | ((g & 255) << 16) | ((r & 255) << 8) | (a & 255);
}

function IntToColor(num) {
    num >>>= 0;
    var a = num & 0xFF,
        r = (num & 0xFF00) >>> 8,
        g = (num & 0xFF0000) >>> 16,
        b = ((num & 0xFF000000) >>> 24);
    return { r, g, b, a }
}

module.exports = { ColorPresentation }