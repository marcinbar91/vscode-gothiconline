const vscode = require('vscode');

class provideHover extends vscode.Hover {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @returns {vscode.Hover}
    */
    provideHover(document, position, token) {
        const commentIndex = document.lineAt(position.line).text.indexOf('//');
        if (commentIndex >= 0 && position.character > commentIndex)
            return undefined;
        const range = document.getWordRangeAtPosition(position);
        if (range == undefined)
            return undefined;
        return new Promise((resolve, reject) => {
            const api = require('../DOCS/apiXml.json')
            const word = document.getText(range)
            for (const iterator of api.functions) {
                if (iterator.name == word) {
                    let hover = new vscode.MarkdownString()
                    hover.appendCodeblock(iterator.declaration, "xml")
                    if (iterator.notes.length > 0)
                        hover.appendMarkdown("***" + iterator.notes[0] + "***\n")
                    hover.appendText(iterator.description)
                    resolve(new vscode.Hover(hover, range))
                }
            }
            resolve(undefined);
        });
    }

}



module.exports = { provideHover }