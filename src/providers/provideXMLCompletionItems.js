const vscode = require('vscode');
const path = require('path')
const files2 = require('../Files')
const utility = require('../utility')
const data = require('../data');

//https://gothicmultiplayerteam.gitlab.io/docs/0.2.1/server-manual/configuration/
class XMLprovideCompletionItems2 extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        const line = document.lineAt(position.line);
        let Idx = line.text.lastIndexOf('<', position.character)
        if (Idx === -1) return ([])
        return new Promise((resolve, reject) => {
            var currentlyOpenTabName = path.dirname(vscode.window.activeTextEditor.document.fileName);
            var tab = [
                this.GetSnippet('<script', 'script src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.nut') + ', type=${2|\"shared\",\"client\",\"server\"|} \/>', "This element specifies script which is loaded when the server/client starts."),
                this.GetSnippet('<import', 'import src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.xml') + ' \/>', "This element specifies the location of import file which will extend current configuration. Elements which are inside import file are using relative path from directory."),
            ]
            if (path.basename(vscode.window.activeTextEditor.document.fileName) == "config.xml") {
                resolve([...tab,
                this.GetSnippet('<config', 'config public="${1|true,false|}" host_name="${2|My server|}" max_slots="${3|32|}" port="${4|28970|}"/>', "This element specifies basic settings for server startup. It is a required element and available only in root configuration file."),
                this.GetSnippet('<debug', 'debug>${1|true,false|}</debug>', "This element enables/disables debug-mode."),
                this.GetSnippet('<world ', 'world name="${1|NEWWORLD\\\\\\\\NEWWORLD.ZEN|}"/>', "This element specifies startup map that will be loaded right after player joined the server. Available only in root configuration file."),
                this.GetSnippet('<version ', 'version build=\"${1|0|}\"/>', "This element specifies minimal required client build version to join the server. Available only in root configuration file."),
                this.GetSnippet('<master', 'master host="${1|master.gothic-online.com.pl|}" port="${2|7777|}" />', "This element specifies master server connection settings. Available only in root configuration file."),
                this.GetSnippet('<description', 'description><![CDATA[\n<center>\n<b><font color=blue>Gothic</font> 2 <font color=red>Online</font> 0.1.10.0</b><br>\n</center>\n]]>\n<\/description>', "This element specifies the server description visible in launcher. Available only in root configuration file."),
                this.GetSnippet('<items', 'items src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.xml') + ' \/>', "This element specifies the location of file that contains Gothic item instances which will be registered by server. Registered items are used to generate identifiers for network optimization."),
                this.GetSnippet('<mds', 'mds src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.xml') + ' \/>', "This element specifies the location of file that contains Gothic mds names which will be registered by server. Registered mds are used to generate identifiers for network optimization."),
                this.GetSnippet('<wayfile', 'wayfile map="$2" src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.xml') + ' \/>', "This element specifies the location of file that contains waypoints and map for which loaded waypoints will be used."),
                this.GetSnippet('<module', 'module src=' + this.GetChoiceSnippets(currentlyOpenTabName, '.dll') + ' type="client" \/>', "This element specifies module which is loaded when the server/client starts. Modules are dynamic-link libraries written in C/C++ to extend possibilities of server."),
                this.GetSnippet('<resource', 'resource vdf="${1|fancy-armors.vdf|}" \/>', "This element specifies required resource to play on the server. Resources are automatically downloaded and loaded in-game."),
                this.GetSnippet('<downloader', 'downloader>\n\t<file-max-chunk>${1|2097152|}</file-max-chunk>\n\t<rate-limit>${2|30|}</rate-limit>\n\t<url>${3|http://localhost:8080|}</url>\n\t<group>${4|MyServer|}</group>\n</downloader>', "This element specifies downloader settings."),
                this.GetSnippet('<streamer', 'streamer>\n\t<radius>${1|3500|}</radius>\n\t<height>${2|2000|}</height>\n\t<intervals>\n\t\t<refresh>${3|500|}</refresh>\n\t\t<info>${4|500|}</info>\n\t</intervals>\n</streamer>', "This element specifies streamer settings. The streamer is cylindrical (πr^2h) and defines in what area other players are visible in relation to their camera position."),
                this.GetSnippet('<synchronization', 'synchronization>\n\t<broadcast-zone-distance>${1|1250|}</broadcast-zone-distance>\n\t<intervals>\n\t\t<player>${2|100|}</player>\n\t\t<camera>${3|500|}</camera>\n\t\t<face-ani>${4|300|}</face-ani>\n\t\t<wear>${5|300|}</wear>\n\t</intervals>\n</synchronization>', "This element specifies synchronization settings. Synchronization specifies milliseconds intervals on how often specified type of packet can be sent to server."),
                this.GetSnippet('<modification', 'modification>\n\t<ikarus>${1|true,false|}</ikarus>\n</modification>', "This element allows you to enable/disable external modifications. Available only in root configuration file."),
                ]);
            }
            else
                resolve(tab)
        });
    }

    GetChoiceSnippets(currentlyOpenTabfileName, ext) {
        var list = "${1|";
        var list2 = utility.searchRecursive(currentlyOpenTabfileName, ext);
        for (let i = 0; i < list2.length; i++) {
            var e = list2[i].split(currentlyOpenTabfileName + '\\').join('')
            if (e != list2[i]) {
                if (path.basename(e) != "config.xml") {
                    list += "\"" + e.replace(/\\/g, "\/") + "\"";
                    if (i < list2.length - 1)
                        list += ","
                }
            }
        }
        list += "|}"
        return list
    }

    GetSnippet(name, snippet, documentation) {
        const snippetCompletion = new vscode.CompletionItem(name);
        //snippetCompletion.kind = new vscode.CompletionItemKind.Snippet
        snippetCompletion.insertText = new vscode.SnippetString(snippet);
        snippetCompletion.documentation = new vscode.MarkdownString(documentation);
        return snippetCompletion
    }
}



class XMLprovideCompletionItems extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        return new Promise((resolve, reject) => {
            let match;
            var currentlyOpenTabfileName = path.dirname(vscode.window.activeTextEditor.document.fileName);
            while (match = data.provideXMLCompletionItemsreg.exec(document.getText())) {
                if (new vscode.Range(document.positionAt(match.index + match[1].length), document.positionAt(match.index + match[0].length - 1)).contains(position)) {
                    if (match[2] == "script") {
                        //let r = new vscode.Range(document.positionAt(match.index + match[1].length), document.positionAt(match.index + match[0].length))
                        resolve(GetCompletion(GetFiles(currentlyOpenTabfileName, '.nut')));
                    } else if (match[2] == "import" || match[2] == "items" || match[2] == "mds" || match[2] == "wayfile") {
                        //let r = new vscode.Range(document.positionAt(match.index + match[1].length), document.positionAt(match.index + match[0].length))
                        resolve(GetCompletion(GetFiles(currentlyOpenTabfileName, '.xml')));
                    }
                }
            }
            resolve([]);
        });
    }
}

class XMLprovideCodeAction extends vscode.CodeAction {
    /**
* @param {vscode.TextDocument} document
* @param {vscode.Range} range
* @param {vscode.CancellationToken} token
* @param {vscode.CompletionContext} context
* @returns {[vscode.CodeAction]}
*/
    provideCodeActions(document, range, token, context) {
        return new Promise((resolve, reject) => {
            let match;
            let a = [];
            while (match = data.provideXMLCompletionItemsreg.exec(document.lineAt(range.start.line).text)) {
                let ran = new vscode.Range(range.start.line, match.index + match[1].length + 1, range.start.line, match.index + match[1].length + match[3].length + 1)
                if (match[2] == "script") {
                    for (const element of GetFiles(path.dirname(document.fileName), '.nut')) {
                        a.push(createFix(document, ran, element.split('\\').join('/')))
                    }
                } else if (match[2] == "import" || match[2] == "items" || match[2] == "mds" || match[2] == "wayfile") {
                    for (const element of GetFiles(path.dirname(document.fileName), '.xml')) {
                        a.push(createFix(document, ran, element.split('\\').join('/')))
                    }
                }
            }
            resolve(a);
        });
    }
}

function GetFiles(currentlyOpenTabfileName, ext) {
    var list = []
    for (const element of files2.GetFilesList(ext)) {
        var e = element.split(currentlyOpenTabfileName + '\\').join('')
        if (element != e) {
            if (path.basename(e) != "config.xml")
                list.push(e);
        }
    }
    return list
}

function GetCompletion(obj) {
    var list = []
    for (const element of obj) {
        var Completion = new vscode.CompletionItem(element.split('\\').join('/'));
        Completion.kind = vscode.CompletionItemKind.Keyword;
        // Completion.insertText = "\""+ Completion.label +"\"";
        // Completion.range = range;
        // Completion.range = {
        //     inserting: range,
        //     replacing: range
        // }
        list.push(Completion)
    }
    return list

}

function createFix(document, range, path) {
    const fix = new vscode.CodeAction(path, vscode.CodeActionKind.QuickFix);
    fix.edit = new vscode.WorkspaceEdit();
    fix.edit.replace(document.uri, range, path);
    return fix;
}

module.exports = { XMLprovideCompletionItems, XMLprovideCompletionItems2, XMLprovideCodeAction }