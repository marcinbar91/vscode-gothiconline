const vscode = require('vscode');
const path = require('path');
const utility = require('../utility');
const files = require('../Files')
const data = require('../data');

//To-DO add support class and static class eg. Camera.setMode()
function getDiagnostic(file) {
    let path2 = file.fileName
    let files2 = files.FindElement(file.fileName)
    let text = file.getText();
    let match;
    let diagnostics = [];
    if (path.extname(path2) === '.nut') {
        while (match = data.DiagnosticReg.exec(text)) {
            for (const element of data.docsDeprected) {
                if (element.name == match[1] && element.side == files2.type) {
                    let temp = utility.matchLineNumber(match);
                    let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                    let diag = new vscode.Diagnostic(range, "Deprecated since version: " + element.deprecated, vscode.DiagnosticSeverity.Warning)
                    diag.tags = [vscode.DiagnosticTag.Deprecated];
                    diagnostics.push(diag);
                }
            }
        }
    }
    return diagnostics;
}

function onDidChangeTextDocument(DeprectedDiagnostic, e) {
    DeprectedDiagnostic.delete(e.document.uri);
    DeprectedDiagnostic.set(e.document.uri, getDiagnostic(e.document));
}

function onDidChangeActiveTextEditor(DeprectedDiagnostic, e) {
    if (e) DeprectedDiagnostic.set(e.document.uri, getDiagnostic(e.document))
}

function onDidCloseTextDocument(DeprectedDiagnostic, doc) {
    DeprectedDiagnostic.delete(doc.uri)
}

module.exports = {
    onDidChangeTextDocument, onDidChangeActiveTextEditor, onDidCloseTextDocument
}

