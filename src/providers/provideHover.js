const vscode = require('vscode');
const item = require('../Items/Item');
const statusbar = require('../statusbar');
const data = require('../data');

class provideHover extends vscode.Hover {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @returns {vscode.Hover}
    */
    provideHover(document, position, token) {
        const commentIndex = document.lineAt(position.line).text.indexOf('//');
        if (commentIndex >= 0 && position.character > commentIndex)
            return undefined;
        const range = document.getWordRangeAtPosition(position);
        if (range == undefined)
            return undefined;
        return new Promise((resolve, reject) => {
            const word = document.lineAt(position.line).text.slice(0, range.end.character).match(/[\w.]+$/g)[0];
            if (statusbar.serverclient.text == "server") {
                let hover = this.mergeHover(data.docs.ServerHover, word, range);
                if (hover)
                    resolve(hover);
            }
            else if (statusbar.serverclient.text == "client") {
                let hover = this.mergeHover(data.docs.ClientHover, word, range);
                if (hover)
                    resolve(hover);
            }
            else {
                let hover = this.mergeHover(data.docs.SharedHover, word, range);
                if (hover)
                    resolve(hover);
            }
            for (const element of data.docs.Constants) {
                for (const element2 of element.hover) {
                    if (word == element2.name) {
                        element2.range = range;
                        resolve(element2);
                    }
                }
            }
            let nonstaticVariable = ""
            let staticVariable = word.split('.')[0]
            var variable = new item.variable(document);
            for (const element of variable.list) {
                if (word.split('.')[0] == element.name) {
                    nonstaticVariable = element.value;
                    break;
                }
            }
            for (const element2 of data.docs.aDOT) {
                if ((nonstaticVariable == element2.name && element2.static == false) || (staticVariable == element2.name && element2.static == true)) {
                    if (element2.side == statusbar.serverclient.text || element2.side == "shared" || statusbar.serverclient.text == undefined) {
                        for (const element3 of element2.Hover) {
                            if (word.split('.')[1] == element3.name.split('.')[1]) {
                                element3.range = range;
                                resolve(element3);
                            }
                        }
                    }
                }
            }

            // if (word.includes('.')) {
            //      var variable = new item.variable(document);
            //      for (const element of variable.list) {
            //          if (word.split('.')[0] == element.name) {
            //              for (const element2 of APIparser.aDOT) {
            //                  if (element.value == element2.name) {
            //                      if (element2.side == statusbar.serverclient.text || element2.side == "shared" || statusbar.serverclient.text == "undefined") {
            //                          for (const element3 of element2.Hover) {
            //                              if (word.split('.')[1] == element3.name.split('.')[1]) {
            //                                  element3.range = range;
            //                                  resolve(element3);
            //                              }
            //                          }
            //                      }
            //                  }
            //              }
            //          }
            //      }
            //  }
            resolve(undefined);
        });
    }

    mergeHover(Hover, word, range) {
        let hover = undefined
        for (const element of Hover) {
            if (word == element.name) {
                if (hover == undefined) {
                    hover = new vscode.Hover(element.contents[0].value, range)
                }
                else {
                    hover.contents[0] += " \n_________________\n" + element.contents[0].value
                    hover.range = range;
                }
            }
        }
        return hover;
    }
}



module.exports = { provideHover }