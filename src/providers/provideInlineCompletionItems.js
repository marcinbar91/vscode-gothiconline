const vscode = require('vscode');
const statusbar = require('../statusbar')
const data = require('../data');

class provideInlineCompletionItems extends vscode.InlineCompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.InlineCompletionContext} context
    * @returns {InlineCompletionItem[] | InlineCompletionList}
    */
    provideInlineCompletionItems(document, position, token, context) {
        return new Promise((resolve, reject) => {
            if (statusbar.serverclient.text == "server") {
                resolve(data.docs.ServerInLine);
            }
            else if (statusbar.serverclient.text == "client") {
                resolve(data.docs.ClientInLine);
            }
            else resolve(data.docs.SharedInLine);
        });
    }
}

module.exports = { provideInlineCompletionItems }