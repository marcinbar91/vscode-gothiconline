const vscode = require('vscode');
const xmlparse = require("fast-xml-parser");
const parser = new xmlparse.XMLParser();
const fs = require("fs");

let Completion = []
let Hover = []

class provideCompletionItems extends vscode.CompletionItem {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @param {vscode.CompletionContext} context
    * @returns {vscode.CompletionList}
    */
    provideCompletionItems(document, position, token, context) {
        return new Promise((resolve, reject) => {
            resolve(Completion);
        });
    }
}
class provideHover extends vscode.Hover {
    /**
    * @param {vscode.TextDocument} document
    * @param {vscode.Position} position
    * @param {vscode.CancellationToken} token
    * @returns {vscode.Hover}
    */
    provideHover(document, position, token) {
        const commentIndex = document.lineAt(position.line).text.indexOf('//');
        if (commentIndex >= 0 && position.character > commentIndex)
            return undefined;
        const range = document.getWordRangeAtPosition(position);
        if (range == undefined)
            return undefined;

        return new Promise((resolve, reject) => {
            const word = document.getText(range)
            for (let i = 0; i < Hover.length; i++) {
                if (Hover[i].name == word) {
                    Hover[i].range = range
                    resolve(Hover[i], range)
                }

            }
            resolve(undefined);
        });
    }
}
function init() {
    Completion = []
    Hover = []
    const file = require("../Files")
    let fileLIST = file.GetFULLFilesList(".xml")
    for (let i = 0; i < fileLIST.length; i++) {
        if (fileLIST[i].tag == "data") {
            let text = fs.readFileSync(fileLIST[i].fullPath).toString();
            let jObj = parser.parse(text).data;
            parseXML(jObj)
        }
    }
}

function parseXML(xml) {
    for (let i = 0; i < xml.items.item.length; i++) {
        Completion.push(getCompletion(xml.items.item[i]))
        Hover.push(getHover(xml.items.item[i]))
    }
    for (let i = 0; i < xml.npcs.npc.length; i++) {
        Completion.push(getCompletion(xml.npcs.npc[i]))
        Hover.push(getHover(xml.npcs.npc[i]))
    }

    for (let i = 0; i < xml.anims.mds.length; i++) {
        for (let j = 0; j < xml.anims.mds[i].ani.length; j++) {
            if (!CheckDuplicate(xml.anims.mds[i].ani[j])) {
                Completion.push(new vscode.CompletionItem(xml.anims.mds[i].ani[j]))
            }
        }
    }
}

function CheckDuplicate(element) {
    for (let i = 0; i < Completion.length; i++) {
        if (Completion[i].label == element)
            return true
    }
    return false;
}

function getCompletion(item) {
    let completion = new vscode.CompletionItem(item.instance)
    completion.documentation = new vscode.MarkdownString()
    completion.documentation.appendCodeblock(GetDocumentation(item), "json")
    return completion;
}

function getHover(item) {
    let doc = new vscode.MarkdownString();
    doc.appendCodeblock(GetDocumentation(item), "json")
    let hover = new vscode.Hover(doc);
    hover.name = item.instance;
    return hover;
}

function GetDocumentation(item) {
    let lines = JSON.stringify(item, null, 4)
    lines = lines.substring(lines.indexOf("\n") + 1);
    lines = lines.substring(lines.lastIndexOf("\n") + 1, -1)
    return lines;
}


module.exports = { provideCompletionItems, provideHover, init }