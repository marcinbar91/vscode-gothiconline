const vscode = require('vscode');
const path = require('path');
const utility = require('../utility');
const files = require('../Files');
const data = require('../data');

function getNumberArg(eventname, type) {
    const doc = data.docs.DOCS2;
    for (const element of doc.events) {
        if ((eventname == element.name)) {
            if (type == element.side || type == undefined) {
                return element.params.length;
            }
        }
    }
    return undefined;
}

function getNumberArgREG(text) {
    var i = 0;
    while (match = data.EventDiagnosticregparams.exec(text)) {
        i++;
    }
    return i;
}

function getDiagnostic(file) {
    let path2 = file.fileName
    let type = files.FindElement(path2).type
    let text = file.getText();
    let match;
    let diagnostics = [];
    if (path.extname(path2) === '.nut') {
        while (match = data.EventDiagnosticreg.exec(text)) {
            let eventname = match[1];
            let RealCountArg = getNumberArg(eventname, type);
            let InCodeCountArg = undefined;
            if (RealCountArg == undefined) {
                let temp = utility.matchLineNumber(match);
                let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                diagnostics.push(new vscode.Diagnostic(range, "Could not find event", vscode.DiagnosticSeverity.Warning));
                continue;
            }
            if (match[2] == undefined) {
                let match2;
                let reg2 = RegExp("(local\\s|static\\s)?function\\s*" + match[3] + "\\s?\\(([^\\)]*)\\)", "gms")
                match2 = reg2.exec(text)
                if (match2 == null) {

                    let temp = utility.matchLineNumber(match);
                    let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                    diagnostics.push(new vscode.Diagnostic(range, "Could not find function " + match[3], vscode.DiagnosticSeverity.Warning));
                    continue;
                }
                else {
                    InCodeCountArg = getNumberArgREG(match2[2]);
                }
            }
            else if (match[2] == '') {
                InCodeCountArg = 0;
            }
            else {
                InCodeCountArg = getNumberArgREG(match[2])
            }
            if (InCodeCountArg != RealCountArg) {
                let temp = utility.matchLineNumber(match);
                let range = new vscode.Range(temp.line - 1, temp.character - 1, temp.line - 1, temp.character + match[0].length - 1);
                diagnostics.push(new vscode.Diagnostic(range, "Incorrect number of arguments", vscode.DiagnosticSeverity.Warning));
                continue;
            }
        }
    }
    return diagnostics;
}


function onDidChangeTextDocument(EventDiagnostic, e) {
    EventDiagnostic.delete(e.document.uri);
    EventDiagnostic.set(e.document.uri, getDiagnostic(e.document));
}

function onDidChangeActiveTextEditor(EventDiagnostic, e) {
    if (e) EventDiagnostic.set(e.document.uri, getDiagnostic(e.document))
}

function onDidCloseTextDocument(EventDiagnostic, doc) {
    EventDiagnostic.delete(doc.uri)
}

module.exports = {
    onDidChangeTextDocument, onDidChangeActiveTextEditor, onDidCloseTextDocument
}

